import { Seeder } from 'mongo-seeding';
import path from 'path';
import dotenv from 'dotenv';
dotenv.config();

const config = {
  database: {
    host: process.env.DB_HOST,
    port: Number(process.env.DB_PORT),
    name: process.env.DB_NAME,
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    options: { authSource: 'admin' }
  },
  dropDatabase: true,
};
const seeder = new Seeder(config);
const collections = seeder.readCollectionsFromPath(
  path.resolve('./seeds/data'), { 
    extensions: ['ts', 'js', 'cjs', 'json'],
    transformers: [
      Seeder.Transformers.replaceDocumentIdWithUnderscoreId,
      Seeder.Transformers.setCreatedAtTimestamp,
      Seeder.Transformers.setUpdatedAtTimestamp
    ]
  });

seeder.import(collections)
  .then(() => { 
    console.log('  \x1b[32m%s\x1b[0m', 'mongo-seeding Seeding data was successfully.');
  })
  .catch((err) => { 
    console.log('  \x1b[31m%s\x1b[0m', 'mongo-seeding Seeding data was failure.'); 
    console.log(err);
    process.exit(1);
  });