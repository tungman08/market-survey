import { getObjectId } from 'mongo-seeding';
import bcrypt from'bcrypt';

const secretAdmin = process.env.SECRET_ADMIN || 'secret';
const password = process.env.SECRET_ADMIN_PASSWORD || 'password';

export = [
  {
    id: getObjectId('user1'),
    username: secretAdmin,
    password: bcrypt.hashSync(password, 10),
    firstName: 'SecretAdmin',
    lastName: 'MarketingSurvey',  
    email: 'admin@localhost.local',
    active: true,
    passwordChanged: true,
    roles: ['SECRET', 'ADMIN']
  }
];

