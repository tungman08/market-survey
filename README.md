# การเตรียมสภาพแวดล้อมของระบบ
1. ### ดาวน์โหลดและติดตั้ง mongodb และ redis
    - ตั้งค่า user และ password ตามต้องการ
    - optional ติดตั้งตัว mongodb client และ redis client
1. ### ดาวน์โหลดและตั้งตั้ง git และ node.js
1. ### ติดตั้ง npm และ yarn
1. ### ตรวจสอบ git, node.js, npm และ yarn
    ```
    git --version
    ```
    ```
    node -v
    ```
    ```
    npm -v
    ```
    ```
    yarn -v
    ```
# การติดตั้งและตั้งค่าระบบ
1. ### โคลนโปรเจ็ค
    ```
    git clone https://gitlab.com/tungman08/market-survey.git
    ```
1. ### ติดตั้ง npm package
    ```
    cd ./market-survey
    yarn install
    ```
1. ### สร้างไฟล์ .env
    ```
    cp .env.example .env
    ```
1. ### แก้ไขค่า config ของ mongodb ใน .env
    - DB_HOST (ตัวอย่าง: localhost)
    - DB_USERNAME (ตัวอย่าง: root)
    - DB_PASSWORD
    - DB_NAME (ตัวอย่าง: mangodb)
1. ### แก้ไขค่า config ของ redis ใน .env
    - REDIS_HOST
    - REDIS_PASSWORD
1. ### แก้ไขค่า config ของ jwt ใน .env
    - JWT_SECRET
    - JWT_EXPIRATION (อายุของ token)
    - JWT_REFRESH_EXPIRATION (อายุของ refresh token)
1. ### แก้ไขค่า config ของ secret admin ใน .env 
    - SECRET_ADMIN (ชื่อ username สำหรับใช้เริ่มต้นในการสร้าง user และใช้ reset password ของ user อื่นๆ)
    - SECRET_ADMIN_PASSWORD
1. ### รัน seed data เพื่อสร้าง secret admin ที่ตั้งค่าไว้
    ```
    yarn seed
    ```
1. ### เมื่อรันแล้ว สามารถเรียกใช้งานได้ ดังนี้ (ดูวิธีการรันใน development mode)
    - เรียกใช้งาน backend ที่ url: http://localhost:3000
    - อ่านคู่มือการใช้งาน backend ที่ url: http://localhost:3000/api-docs
    - เข้าระบบด้วย
        > username: \<ชื่อ secret admin ที่ตั้งไว้>

        > password: \<secret admin password ที่ตั้งไว้>
# การใช้งานสำหรับ development mode
1. ### คำสั่งในการทดสอบ

    ```
    yarn test
    ```
    หรือรันเฉพาะส่วนที่ต้องการทดสอบ (ในตัวอย่างจะเทส user.service และ user.controller)
    ```
    yarn test user
    ```
    หรือรันโดยระบุที่ชื่อของ test ที่ต้องการ
    ```
    yarn test user.service.spec.ts
    ```
1. ### คำสั่งในการรัน
    ```
    yarn build
    yarn start
    ```
    หรือรันในโหมดพัฒนาด้วยคำสั่ง
    ```
    yarn watch
    ```
# การติดตั้งบน production
1. ### ติดตั้ง pm2
    ```
    npm install pm2 -g
    ```
1. ### คำสั่งในการบิวด์
    ```
    yarn build
    ```
1. ### คำสั่งในการรัน service โดยจะสร้าง instance ขึ้นมา 2 instance
    ```
    pm2 start dist/main.js -i 2
    ```
1. ### คำสั่ง pm2 อื่น ๆ
    ```
    pm2 list
    pm2 monit
    pm2 start <all | name (ex: main) | index (ex: 0)>
    pm2 stop <all | name (ex: main) | index (ex: 0)>
    pm2 restart <all | name (ex: main) | index (ex: 0)>
    pm2 delete <all | name (ex: main) | index (ex: 0)>
    ```
1. ### ทำ reverse proxy ด้วย nginx เพื่อเปลี่ยนจาก port: 3000 ให้เป็น port: 80 (ในตัวอย่างจะติดตั้งพร้อมตัว frontend ซึ่งสมมุติว่า build ไว้ที่ '../../market-survey-vue/dist')
    <code>
    
        server {
            listen       80;

            location ^~ /api {
                rewrite           ^/api/(.*)$  /$1  break;
                proxy_set_header  X-Forwarded-For  $remote_addr;
                proxy_set_header  Host  $http_host;
                proxy_pass        http://localhost:3000;
            } 

            location / {
                root              ../../market-survey-vue/dist;
                try_files         $uri  $uri/  /index.html;
            }  
        }  
    </code>
1. ### สามารถเรียกผ่าน port: 80 ดังนี้
    - ตัว frontend -> http://\<IP Address>/
    - ตัว backend -> http://\<IP Address>/api/
    - ตัว api-docs -> http://\<IP Address>/api-docs/
