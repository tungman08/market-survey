export const validateNotEmpty = (received: any) => {
  expect(received).not.toBeNull();
  expect(received).not.toBeUndefined();
  expect(received).toBeTruthy();
};

export const validateEmpty = (received: any) => {
  expect(received).toBeNull();
  expect(received).not.toBeUndefined();
  expect(received).not.toBeTruthy();
};

export const validateMongoError = (name: any, code: any) => {
  expect(name).toMatch(/Error/i);
  expect(code).toBe(11000);
};
