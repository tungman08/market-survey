export const validateQuestionEquality = (received: any, expected: any) => {
  expect(received.required).toEqual(expected.required);
  expect(received.questionType).toEqual(expected.questionType);
  expect(received.questionBody).toEqual(expected.questionBody);
};
