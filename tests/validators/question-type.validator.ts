export const validateQuestionTypeEquality = (received: any, expected: any) => {
  expect(received).toHaveLength(5);

  for (let i = 0; i < received.length; i++) {
    expect(received[i].name).toEqual(expected[i].name);
    expect(received[i].value).toEqual(expected[i].value);
  } 
};
