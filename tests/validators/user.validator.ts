import { UserDocument } from '../../src/models/user.model';
import { MockCreateUser, MockUpdateUser } from '../mocks/user.mock';

export const validateUserEquality = (received: UserDocument, expected: any) => {
  expect(received.username).toEqual(expected.username ?? MockCreateUser.username);
  expect(received.firstName).toEqual(expected.firstName);
  expect(received.lastName).toEqual(expected.lastName);
  expect(received.email).toEqual(expected.email);
  expect(received.roles).toHaveLength((expected.roles as string[]).length); 
  expect(received.active).toEqual(expected.active ?? MockUpdateUser.active);
};
