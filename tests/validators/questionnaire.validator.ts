export const validateQuestionnaireEquality = (received: any, expected: any) => {
  expect(received.title).toEqual(expected.title);
  expect(received.description).toEqual(expected.description);
};