// Answer Validate
export const validateAnswerEquality = (received: any, expected: any) => {
  expect(received.question).not.toBeNull();
  expect(received.question).not.toBeUndefined();
  expect(received.value).not.toBeNull();
  expect(received.value).toEqual(expected.value);
};