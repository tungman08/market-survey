import { Types } from 'mongoose';
import { Questionnaire } from '../../src/models/questionnaire.model';
import questionService from '../../src/services/question.service';
import { Respondent } from '../../src/models/respondent.model';
import answerService from '../../src/services/answer.service';
import { DbConnect, DbDisconnect } from '../configs/mongodb.config';
import { MockCreateQuestionnaire } from '../mocks/questionnaire.mock';
import { MockCreateQuestions } from '../mocks/question.mock';
import { MockCreateAnswers, MockUpdateAnswers, FakeId } from '../mocks/answer.mock';
import { validateNotEmpty, validateEmpty } from '../validators/utilities';
import { validateAnswerEquality } from '../validators/answer.validator';


let questionnaireId: string;
let respondentId: string;
const questionIdArray: string[] = [];
const idArray: string[] = [];
beforeAll(async () => { 
  await DbConnect();

  const questionnaire = new Questionnaire(MockCreateQuestionnaire);
  await questionnaire.save();
  questionnaireId = questionnaire.id;

  await Promise.all(MockCreateQuestions.map(async (payload) => {
    const question = await questionService.Create(questionnaireId, payload);
    questionIdArray.push(question.id);
  }));

  const respondent = new Respondent({ questionnaire: questionnaireId });
  await respondent.save();
  respondentId = respondent.id;
});
afterAll(async () => { await Questionnaire.deleteOne({ _id: Types.ObjectId(questionnaireId) }); await DbDisconnect(); });


/**
 * Creating Test
 */
describe('Answer Service: Creating Test', () => {
  test('should successfully and return answer if response text', async () => {
    const answer = await answerService.Create(respondentId, questionIdArray[0], MockCreateAnswers[0]);
    idArray.push(answer.id);
    validateNotEmpty(answer);
    validateAnswerEquality(answer, MockCreateAnswers[0]);
  });

  test('should successfully and return answer if response option', async () => {
    const answer = await answerService.Create(respondentId, questionIdArray[1], MockCreateAnswers[1]);
    idArray.push(answer.id);
    validateNotEmpty(answer);
    validateAnswerEquality(answer, MockCreateAnswers[1]);
  }); 

  test('should successfully and return answer if response rating', async () => {
    const answer = await answerService.Create(respondentId, questionIdArray[2], MockCreateAnswers[2]);
    idArray.push(answer.id);
    validateNotEmpty(answer);
    validateAnswerEquality(answer, MockCreateAnswers[2]);
  }); 

  test('should successfully and return answer if response option grid', async () => {
    const answer = await answerService.Create(respondentId, questionIdArray[3], MockCreateAnswers[3]);
    idArray.push(answer.id);
    validateNotEmpty(answer);
    validateAnswerEquality(answer, MockCreateAnswers[3]);
  }); 

  test('should successfully and return answer if response datetime', async () => {
    const answer = await answerService.Create(respondentId, questionIdArray[4], MockCreateAnswers[4]);
    idArray.push(answer.id);
    validateNotEmpty(answer);
    validateAnswerEquality(answer, MockCreateAnswers[4]);
  });
});


/**
 * Getting Test
 */
describe('Answer Service: Getting Test', () => {
  test('should successfully and return answers', async () => {
    const answers = await answerService.Fetch(respondentId, questionIdArray[0]);
    validateNotEmpty(answers);
    expect(answers.length).toEqual(1);
  });
});


/**
 * Finding Test
 */
describe('Answer Service: Finding Test', () => {
  test('should successfully and return answer', () => {
    let index = 0;
    idArray.forEach(async (id) => {
      const answer =  await answerService.FindById(id);
      validateNotEmpty(answer);
      validateAnswerEquality(answer, MockCreateAnswers[index++]);
    });
  });

  test('should successfully and return answer if find by respondent and question', async () => {
    const answer = await answerService.FindByRespondentAndQuestion(respondentId, questionIdArray[0]);
    validateNotEmpty(answer);
    validateAnswerEquality(answer, MockCreateAnswers[0]);
  });

  test('should successfully and return null if not found', async () => {
    const answer =  await answerService.FindById(FakeId);
    validateEmpty(answer);
  });
});


/**
 * Updating Test
 */
describe('Answer Service: Updating Test', () => {
  test('should successfully and return answer if update textbox answer', async () => {
    const answer = await answerService.Update(idArray[0], MockUpdateAnswers[0]);
    validateNotEmpty(answer);
    if (answer) {
      expect(answer.value).toEqual(MockUpdateAnswers[0].value);
    }
  });

  test('should successfully and return answer if update option answer', async () => {
    const answer = await answerService.Update(idArray[1], MockUpdateAnswers[1]);
    validateNotEmpty(answer);
    if (answer) {
      expect(answer.value).toEqual(MockUpdateAnswers[1].value);
    }
  }); 

  test('should successfully and return answer if update rating answer', async () => {
    const answer = await answerService.Update(idArray[2], MockUpdateAnswers[2]);
    validateNotEmpty(answer);
    if (answer) {
      expect(answer.value).toEqual(MockUpdateAnswers[2].value);
    }
  }); 

  test('should successfully and return answer if update option grid answer', async () => {
    const answer = await answerService.Update(idArray[3], MockUpdateAnswers[3]);
    validateNotEmpty(answer);
    if (answer) {
      expect(answer.value).toEqual(MockUpdateAnswers[3].value);
    }
  }); 

  test('should successfully and return answer if update datetime answer', async () => {
    const answer = await answerService.Update(idArray[4], MockUpdateAnswers[4]);
    validateNotEmpty(answer);
    if (answer) {
      expect(answer.value).toEqual(MockUpdateAnswers[4].value);
    }
  });
});


/**
 * Deleting Test
 */
describe('Answer Service: Deleting Test', () => {
  test('should successfully if not found', async () => {
    await answerService.Delete(FakeId);
    const answer = await answerService.FindById(FakeId);
    validateEmpty(answer);
  });

  test('should successfully and not return question', () => {
    idArray.forEach(async (id) => {
      await answerService.Delete(id);
      const answer =  await answerService.FindById(id);
      validateEmpty(answer);
    });
  });
});
