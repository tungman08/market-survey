import dotenv from 'dotenv'; dotenv.config();
import { User, UserDocument } from '../../src/models/user.model';
import userService from '../../src/services/user.service';
import { DbConnect, DbDisconnect } from '../configs/mongodb.config';
import { MockCreateUser, MockUpdateUser, MockSecretAdmin, FakeId, FakeUserName } from '../mocks/user.mock';
import { validateUserEquality } from '../validators/user.validator';
import { validateNotEmpty, validateEmpty, validateMongoError } from '../validators/utilities';


let id: string;
beforeAll(async () => { await DbConnect(); const secret = new User(MockSecretAdmin); secret.save(); });
afterAll(async () => await DbDisconnect());


/**
 * Creating Test
 */
describe('User Service: Creating Test', () => {
  test('should successfully and return user', async () => {
    const user = await userService.Create(MockCreateUser);
    id = user.id;
    validateNotEmpty(user);
    validateUserEquality(user, MockCreateUser);
  });

  test('should error if duplicate', async () => {
    try {
      await userService.Create(MockCreateUser);
    } catch (error) {
      validateMongoError(error.name, error.code);
    }
  });

  test('should error if invalid data', async () => {
    try {
      await userService.Create({});
    } catch (error) {
      expect(error.name).toMatch(/Error/i);
    }
  });
});


/**
 * Getting Test
 */
describe('User Service: Getting Test', () => {
  test('should successfully and return all users', async () => {
    const users = await userService.Fetch();
    validateNotEmpty(users);
    expect(users).toHaveLength(1);

    if (users.length > 0) {
      expect(users[0].username).not.toEqual('secret');
    }
  });
});


/**
 * Finding Test
 */
describe('User Service: Finding Test', () => {
  test('should successfully and return user if find by id', async () => {
    const user = await userService.FindById(id);
    validateNotEmpty(user);
    validateUserEquality(user as UserDocument, MockCreateUser);
  });

  test('should successfully with return null if not found', async () => {
    const user = await userService.FindById(FakeId);
    validateEmpty(user);
  });

  test('should successfully and return user if find by username', async () => {
    const user = await userService.FindByUsername(MockCreateUser.username);
    validateNotEmpty(user);
    validateUserEquality(user as UserDocument, MockCreateUser);
  });

  test('should successfully and return null if username not found', async () => {
    const user =  await userService.FindByUsername(FakeUserName);
    validateEmpty(user);
  });
});


/**
 * Updating Test
 */
describe('User Service: Updating Test', () => {
  test('should successfully and return Updated user', async () => {
    const user = await userService.Update(id, MockUpdateUser);
    validateNotEmpty(user);
    validateUserEquality(user as UserDocument, MockUpdateUser);
  });

  test('should successfully and return null if not found', async () => {
    const user = await userService.Update(FakeId, MockUpdateUser);
    validateEmpty(user);
  });

  test('should error if invalid data', async () => {
    try {
      await userService.Update(id, {});
    } catch (error) {
      expect(error.name).toMatch(/Error/i);
    }
  });
});


/**
 * Deleting Test
 */
describe('User Service: Deleting Test', () => {
  test('should successfully and user exist if not found', async () => {
    await userService.Delete(FakeId);
    const user = await userService.FindById(FakeId);
    validateEmpty(user);
  });

  test('should successfully and not return user', async () => {
    await userService.Delete(id);
    const user = await userService.FindById(FakeId);
    validateEmpty(user);
  });
});
