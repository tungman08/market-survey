import userAgentService from '../../src/services/userAgent.service';
import { UaString1, UaString2, UaString3, UaString4 } from '../mocks/userAgent.mock';

/**
 * User Agent Service Test
 */
describe('User Agent Service Test', () => {
  test('should be Chromium', async () => {
    const result = userAgentService.Parser(UaString1);
    expect(result.browser.name).toEqual('Chromium');
  });

  test('should be Konqueror', async () => {
    const result = userAgentService.Parser(UaString2);
    expect(result.browser.name).toEqual('Konqueror');
  });

  test('should be Firefox', async () => {
    const result = userAgentService.Parser(UaString3);
    expect(result.browser.name).toEqual('Firefox');
  });

  test('should be Safari', async () => {
    const result = userAgentService.Parser(UaString4);
    expect(result.browser.name).toEqual('Safari');
  });
});