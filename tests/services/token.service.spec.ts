import dotenv from 'dotenv'; dotenv.config();
import { Types } from 'mongoose';
import { User } from '../../src/models/user.model';
import tokenService from '../../src/services/token.service';
import userAgentService from '../../src/services/userAgent.service';
import { DbConnect, DbDisconnect } from '../configs/mongodb.config';
import { RdClient } from '../configs/redis.config';
import { UaString1 } from '../mocks/userAgent.mock';
import { MockUser } from '../mocks/user.mock';


beforeAll(async () => await DbConnect());
afterAll(async () => await DbDisconnect());


/**
 * Token Service Test
 */
describe('Token Service Test', () => {
  const redis = RdClient();
  const userAgent = userAgentService.Parser(UaString1);
  const browserName = userAgent.browser.name as string;
  let userId: string;
  beforeAll(async () => { const user = new User(MockUser); await user.save(); userId = user.id; });
  afterAll(async () => await User.deleteOne({ _id: Types.ObjectId(userId) }));

  test('creating token should successfully', async () => {
    const token = await tokenService.Create(redis, '127.0.0.1', browserName, userId);
    expect(token.accessToken).not.toBeNull();
    expect(token.refreshToken).not.toBeNull();
    expect(token.expiresIn).toBeGreaterThan(0);
    expect(token.refreshExpiresIn).toBeGreaterThan(0);
  });

  test('finding token should successfully', async () => {
    const token = await tokenService.Find(redis, '127.0.0.1', browserName, userId);
    expect(token.accessToken).not.toBeNull();
    expect(token.refreshToken).not.toBeNull();
    expect(token.expiresIn).toBeGreaterThan(0);
    expect(token.refreshExpiresIn).toBeGreaterThan(0);
  });

  test('revoking token should successfully with null', async () => {
    await tokenService.Revoke(redis, '127.0.0.1', browserName, userId);
    const token = await tokenService.Find(redis, '127.0.0.1', browserName, userId);
    expect(token.accessToken).toBeNull();
    expect(token.refreshToken).toBeNull();
    expect(token.expiresIn).not.toBeGreaterThan(0);
    expect(token.refreshExpiresIn).not.toBeGreaterThan(0);
  });
});
