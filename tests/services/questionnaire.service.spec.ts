import { DbConnect, DbDisconnect } from '../configs/mongodb.config';
import questionnaireService from '../../src/services/questionnaire.service';
import { User, UserDocument } from '../../src/models/user.model';
import { MockUser } from '../mocks/user.mock';
import { MockCreateQuestionnaire, MockUpdateQuestionnaire, FakeId } from '../mocks/questionnaire.mock';
import { validateNotEmpty, validateEmpty } from '../validators/utilities';
import { validateQuestionnaireEquality } from '../validators/questionnaire.validator';


let id: string;
let user: UserDocument;
beforeAll(async () => { await DbConnect(); user = new User(MockUser); user.save(); });
afterAll(async () => await DbDisconnect());


/** 
 * Creating Test
 */
describe('Questionnaire Service: Creating Test', () => {
  test('should successfully and return questionnaire', async () => {
    const questionnaire = await questionnaireService.Create(user.id, MockCreateQuestionnaire);
    id = questionnaire.id;

    validateNotEmpty(questionnaire);
    validateQuestionnaireEquality(questionnaire, MockCreateQuestionnaire);
  });

  test('should error if invalid data', async () => {
    try {
      await questionnaireService.Create(user.id, {});
    } catch (error) {
      expect(error.name).toMatch(/Error/i);
    }
  });
});


/**
 * Getting Questionnaire Test
 */
 describe('Questionnaire Service: Getting Questionnaire Test', () => {
  test('should successfully and return questionnaire with quesions and respondents', async () => {
    const questionnaire = await questionnaireService.GetResult(id);
    validateNotEmpty(questionnaire);
    if (questionnaire) {
      expect(questionnaire.questions).toHaveLength(0);
      expect(questionnaire.respondents).toHaveLength(0);
    }
  });

  test('should successfully and return questionnaire with quesions', async () => {
    const questionnaire = await questionnaireService.GetForm(id);
    validateNotEmpty(questionnaire);
    if (questionnaire) {
      expect(questionnaire.questions).toHaveLength(0);
    }
  });
});


/**
 * Getting Test
 */
describe('Questionnaire Service: Getting Test', () => {
  test('should successfully and return all questionnaires', async () => {
    const questionnaires = await questionnaireService.Fetch(user.id);
    validateNotEmpty(questionnaires);
    expect(questionnaires).toHaveLength(1);
  });
});


/**
 * Finding Test
 */
describe('Questionnaire Service: Finding Test', () => {
  test('should successfully and return questionnaire if find by id', async () => {
    const questionnaire = await questionnaireService.FindById(id);
    validateNotEmpty(questionnaire);
    validateQuestionnaireEquality(questionnaire, MockCreateQuestionnaire);
  });

  test('should successfully with return null if not found', async () => {
    const questionnaire = await questionnaireService.FindById(FakeId);
    validateEmpty(questionnaire);
  });
});


/**
 * Updating Test
 */
describe('Questionnaire Service: Updating Test', () => {
  test('should successfully and return questionnaire', async () => {
    const questionnaire = await questionnaireService.Update(id, MockUpdateQuestionnaire);
    validateNotEmpty(questionnaire);
    validateQuestionnaireEquality(questionnaire, MockUpdateQuestionnaire);
  });

  test('should successfully and return null if not found', async () => {
    const questionnaire = await questionnaireService.Update(FakeId, MockUpdateQuestionnaire);
    validateEmpty(questionnaire);
  });

  test('should error if invalid data', async () => {
    try {
      await questionnaireService.Update(id, {});
    } catch (error) {
      expect(error.name).toMatch(/Error/i);
    }
  });
});


/**
 * Deleting Test
 */
describe('Questionnaire Service: Deleting Test', () => {
  test('should successfully and questionnaire exist if not found', async () => {
    await questionnaireService.Delete(FakeId);
    const questionnaire = await questionnaireService.FindById(FakeId);
    validateEmpty(questionnaire);
  });

  test('should successfully and not return questionnaire', async () => {
    await questionnaireService.Delete(id);
    const questionnaire = await questionnaireService.FindById(id);
    validateEmpty(questionnaire);
  });
});
