import { Types } from 'mongoose';
import { Questionnaire } from '../../src/models/questionnaire.model';
import respondentService from '../../src/services/respondent.service';
import { DbConnect, DbDisconnect } from '../configs/mongodb.config';
import { MockCreateQuestionnaire, FakeId } from '../mocks/questionnaire.mock';
import { validateNotEmpty, validateEmpty } from '../validators/utilities';


let id: string;
let questionnaireId: string;
beforeAll(async () => { await DbConnect(); const questionnaire = new Questionnaire(MockCreateQuestionnaire); await questionnaire.save(); questionnaireId = questionnaire.id; });
afterAll(async () => { await Questionnaire.deleteOne({ _id: Types.ObjectId(questionnaireId) }); await DbDisconnect(); });


/**
 * Creating Test
 */
describe('Respondent Service: Creating Test', () => {
  test('should successfully and return respondent', async () => {
    const respondent = await respondentService.Create(questionnaireId, {});
    id = respondent.id;
    validateNotEmpty(respondent);
  });
});


/**
 * Getting Test
 */
describe('Respondent Service: Getting Test', () => {
  test('should successfully and return respondents', async () => {
    const respondents = await respondentService.Fetch(questionnaireId);
    validateNotEmpty(respondents);
    expect(respondents.length).toEqual(1);
  });
});


/**
 * Finding Test
 */
describe('Respondent Service: Finding Test', () => {
  test('should successfully and return respondent', async () => {
    const respondent = await respondentService.FindById(id);
    validateNotEmpty(respondent);
  });

  test('should successfully and return null if not found', async () => {
    const answer =  await respondentService.FindById(FakeId);
    validateEmpty(answer);
  });
});


/**
 * Deleting Test
 */
describe('Respondent Service: Deleting Test', () => {
  test('should successfully if not found', async () => {
    await respondentService.Delete(FakeId);
    const respondent = await respondentService.FindById(FakeId);
    validateEmpty(respondent);
  });

  test('should successfully and not return question', async () => {
    await respondentService.Delete(id);
    const respondent = await respondentService.FindById(id);
    validateEmpty(respondent);
  });
});
