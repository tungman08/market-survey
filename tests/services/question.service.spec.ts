import { Types } from 'mongoose';
import { Questionnaire } from '../../src/models/questionnaire.model';
import questionService from '../../src/services/question.service';
import { DbConnect, DbDisconnect } from '../configs/mongodb.config';
import { User } from '../../src/models/user.model';
import { MockUser } from '../mocks/user.mock';
import { MockCreateQuestionnaire } from '../mocks/questionnaire.mock';
import { MockCreateQuestions, MockUpdateQuestions, MockInvalidQuestion, FakeQuestionnaireId, FakeId } from '../mocks/question.mock';
import { validateNotEmpty, validateEmpty } from '../validators/utilities';
import { validateQuestionEquality } from '../validators/question.validator';


let questionnaireId: string;
const idArray: string[] = [];
const scoreArray: number[] = [];
beforeAll(async () => { 
  await DbConnect(); 
  const user = new User(MockUser);
  user.save();
  
  const questionnaire = new Questionnaire({ user: user.id, ...MockCreateQuestionnaire }); 
  await questionnaire.save(); 
  questionnaireId = questionnaire.id;
});
afterAll(async () => { await Questionnaire.deleteOne({ _id: Types.ObjectId(questionnaireId) }); await DbDisconnect(); });


/**
 * Creating Test
 */
describe('Question Service: Creating Test', () => {
  test('should successfully and return question if add textbox', async () => {
    const question = await questionService.Create(questionnaireId, MockCreateQuestions[0]);
    idArray.push(question.id);
    scoreArray.push(question.score);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockCreateQuestions[0]);
  });

  test('should successfully and return question if add option', async () => {
    const question = await questionService.Create(questionnaireId, MockCreateQuestions[1]);
    idArray.push(question.id);
    scoreArray.push(question.score);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockCreateQuestions[1]);
  }); 

  test('should successfully and return question if add rating', async () => {
    const question = await questionService.Create(questionnaireId, MockCreateQuestions[2]);
    idArray.push(question.id);
    scoreArray.push(question.score);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockCreateQuestions[2]);
  }); 

  test('should successfully and return question if add option grid', async () => {
    const question = await questionService.Create(questionnaireId, MockCreateQuestions[3]);
    idArray.push(question.id);
    scoreArray.push(question.score);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockCreateQuestions[3]);
  }); 

  test('should successfully and return question if add datetime', async () => {
    const question = await questionService.Create(questionnaireId, MockCreateQuestions[4]);
    idArray.push(question.id);
    scoreArray.push(question.score);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockCreateQuestions[4]);
  });

  test('should error if require fields are empty', async () => {
    try {
      await questionService.Create(questionnaireId, MockInvalidQuestion);
    } catch (error) {
      expect(error.name).toMatch(/Error/i);
    }
  }); 
});


/**
 * Getting Test
 */
describe('Question Service: Getting Test', () => {
  test('should successfully and return sorted all questions in questionnaire', async () => {
    const questions = await questionService.Fetch(questionnaireId);
    validateNotEmpty(questions);
    expect(questions.length).toEqual(MockCreateQuestions.length);
  });

  test('should successfully and return amount questions in questionnaire', async () => {
    const count = await questionService.Count(questionnaireId);
    expect(count).toEqual(MockCreateQuestions.length);
  });

  test('should successfully and return empty array if not found', async () => {
    const questions = await questionService.Fetch(FakeQuestionnaireId);
    expect(questions.length).toEqual(0);
  });
});


/**
 * Finding Test
 */
describe('Question Service: Finding Test', () => {
  test('should successfully and return textbox question if find by id', async () => {
    const question =  await questionService.FindById(idArray[0]);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockCreateQuestions[0]);
  });

  test('should successfully and return option question if find by id', async () => {
    const question =  await questionService.FindById(idArray[1]);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockCreateQuestions[1]);
  });

  test('should successfully and return rating question if find by id', async () => {
    const question =  await questionService.FindById(idArray[2]);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockCreateQuestions[2]);
  });

  test('should successfully and return option-grid question if find by id', async () => {
    const question =  await questionService.FindById(idArray[3]);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockCreateQuestions[3]);
  });

  test('should successfully and return datetime question if find by id', async () => {
    const question =  await questionService.FindById(idArray[4]);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockCreateQuestions[4]);
  });

  test('should successfully and return null if not found', async () => {
    const question =  await questionService.FindById(FakeId);
    validateEmpty(question);
  });

  test('should successfully and return textbox question if find by score', async () => {
    const question =  await questionService.FindByScore(questionnaireId, scoreArray[0]);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockCreateQuestions[0]);
  });

  test('should successfully and return option question if find by score', async () => {
    const question =  await questionService.FindByScore(questionnaireId, scoreArray[1]);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockCreateQuestions[1]);
  });

  test('should successfully and return rating question if find by score', async () => {
    const question =  await questionService.FindByScore(questionnaireId, scoreArray[2]);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockCreateQuestions[2]);
  });

  test('should successfully and return option-grid question if find by score', async () => {
    const question =  await questionService.FindByScore(questionnaireId, scoreArray[3]);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockCreateQuestions[3]);
  });

  test('should successfully and return datetime question if find by score', async () => {
    const question =  await questionService.FindByScore(questionnaireId, scoreArray[4]);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockCreateQuestions[4]);
  });

  test('should successfully and return null if score not found', async () => {
    const question =  await questionService.FindByScore(questionnaireId, 0);
    validateEmpty(question);
  });
});


/**
 * Updating Test
 */
describe('Question Service: Updating Test', () => {
  test('should successfully and return question if update textbox', async () => {
    const question = await questionService.Update(idArray[0], MockUpdateQuestions[0]);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockUpdateQuestions[0]);
  });

  test('should successfully and return question if update option', async () => {
    const question = await questionService.Update(idArray[1], MockUpdateQuestions[1]);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockUpdateQuestions[1]);
  }); 

  test('should successfully and return question if update rating', async () => {
    const question = await questionService.Update(idArray[2], MockUpdateQuestions[2]);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockUpdateQuestions[2]);
  }); 

  test('should successfully and return question if update option grid', async () => {
    const question = await questionService.Update(idArray[3], MockUpdateQuestions[3]);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockUpdateQuestions[3]);
  }); 

  test('should successfully and return question if update datetime', async () => {
    const question = await questionService.Update(idArray[4], MockUpdateQuestions[4]);
    validateNotEmpty(question);
    validateQuestionEquality(question, MockUpdateQuestions[4]);
  });

  test('should error if require fields are empty', async () => {
    try {
      await questionService.Update(idArray[0], MockInvalidQuestion);
    } catch (error) {
      expect(error.name).toMatch(/Error/i);
    }
  }); 
});


/**
 * Deleting Test
 */
describe('Question Service: Deleting Test', () => {
  test('should successfully if not found', async () => {
    await questionService.Delete(FakeId);
    const question = await questionService.FindById(FakeId);
    validateEmpty(question);
  });

  test('should successfully and not return question if delete textbox', async () => {
      await questionService.Delete(idArray[0]);
      const question =  await questionService.FindById(idArray[0]);
      validateEmpty(question);
  });
  
  test('should successfully and not return question if delete option', async () => {
    await questionService.Delete(idArray[1]);
    const question =  await questionService.FindById(idArray[1]);
    validateEmpty(question);
  });
  
  test('should successfully and not return question if delete rating', async () => {
    await questionService.Delete(idArray[2]);
    const question =  await questionService.FindById(idArray[2]);
    validateEmpty(question);
  });
  
  test('should successfully and not return question if delete option-grid', async () => {
    await questionService.Delete(idArray[3]);
    const question =  await questionService.FindById(idArray[3]);
    validateEmpty(question);
  });
  
  test('should successfully and not return question if delete datetime', async () => {
    await questionService.Delete(idArray[4]);
    const question =  await questionService.FindById(idArray[4]);
    validateEmpty(question);
  });
});
