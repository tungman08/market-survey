import mongoose from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
const mongoServer = new MongoMemoryServer();

export const DbConnect = async (): Promise<void> => {
  const uri = await mongoServer.getUri();
  const mongooseOpts = {
    useNewUrlParser: true,
    useCreateIndex: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  };

  await mongoose.connect(uri, mongooseOpts);
};

export const DbDisconnect = async (): Promise<void> => {
  await mongoose.connection.dropDatabase();
  await mongoose.connection.close();
  await mongoServer.stop();
};
