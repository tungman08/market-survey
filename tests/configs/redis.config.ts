import MockRedis, { Redis } from 'ioredis-mock';

export const RdClient = (): Redis => {
  const client = new MockRedis({});
  return client;
};
