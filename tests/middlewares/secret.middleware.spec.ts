import { getMockReq, getMockRes } from '@jest-mock/express';
import { User, UserDocument } from '../../src/models/user.model';
import { DbConnect, DbDisconnect } from '../configs/mongodb.config';
import { RdClient } from '../configs/redis.config';
import secretMiddleware from '../../src/middlewares/secret.middleware';
import { UaString1 } from '../mocks/userAgent.mock';
import { MockAdmin, MockSecretAdmin } from '../mocks/user.mock';


const { res, next, mockClear } = getMockRes();
let admin: UserDocument;
let secret: UserDocument;
beforeAll(async () => { 
  await DbConnect(); 
  admin = new User(MockAdmin);
  admin.save();

  secret = new User(MockSecretAdmin);
  secret.save();
});
afterAll(async () => await DbDisconnect());


/**
 * Filtering Test
 */
describe('Secret Admin Middleware: Filtering Test', () => {
  afterEach(() => mockClear());

  test('should error if user not has secret admin role', async () => {
    const req = getMockReq({
      connection: { remoteAddress: '127.0.0.1' },
      headers: { 'user-agent': UaString1 },
      redis: RdClient(),
      user: admin
    });

    await secretMiddleware(req, res, next);
    expect(res.status).toHaveBeenCalledWith(401);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: 'You don\'t have authorize for secret section' });
  });

  test('should successfully if user has secret admin role', async () => {
    const req = getMockReq({
      connection: { remoteAddress: '127.0.0.1' },
      headers: { 'user-agent': UaString1 },
      redis: RdClient(),
      user: secret
    });

    await secretMiddleware(req, res, next);
    expect(next).toHaveBeenCalled();
  });
});
