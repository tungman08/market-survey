import { getMockReq, getMockRes } from '@jest-mock/express';
import { User, UserDocument } from '../../src/models/user.model';
import { DbConnect, DbDisconnect } from '../configs/mongodb.config';
import { RdClient } from '../configs/redis.config';
import adminMiddleware from '../../src/middlewares/admin.middleware';
import { UaString1 } from '../mocks/userAgent.mock';
import { MockUser, MockAdmin } from '../mocks/user.mock';


const { res, next, mockClear } = getMockRes();
let user: UserDocument;
let admin: UserDocument;
beforeAll(async () => { 
  await DbConnect(); 
  user = new User(MockUser);
  user.save();

  admin = new User(MockAdmin);
  admin.save();
});
afterAll(async () => await DbDisconnect());


/**
 * Filtering Test
 */
describe('Admin Middleware: Filtering Test', () => {
  afterEach(() => mockClear());

  test('should error if user not has admin role', async () => {
    const req = getMockReq({
      connection: { remoteAddress: '127.0.0.1' },
      headers: { 'user-agent': UaString1 },
      redis: RdClient(),
      user: user
    });

    await adminMiddleware(req, res, next);
    expect(res.status).toHaveBeenCalledWith(401);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: 'You don\'t have authorize for admin section' });
  });

  test('should successfully if user has admin role', async () => {
    const req = getMockReq({
      connection: { remoteAddress: '127.0.0.1' },
      headers: { 'user-agent': UaString1 },
      redis: RdClient(),
      user: admin
    });

    await adminMiddleware(req, res, next);
    expect(next).toHaveBeenCalled();
  });
});
