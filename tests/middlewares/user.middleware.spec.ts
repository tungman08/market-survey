import { getMockReq, getMockRes } from '@jest-mock/express';
import { User, UserDocument } from '../../src/models/user.model';
import { DbConnect, DbDisconnect } from '../configs/mongodb.config';
import { RdClient } from '../configs/redis.config';
import userMiddleware from '../../src/middlewares/user.middleware';
import { UaString1 } from '../mocks/userAgent.mock';
import { MockUser, MockAdmin } from '../mocks/user.mock';


const { res, next, mockClear } = getMockRes();
let user: UserDocument;
let admin: UserDocument;
beforeAll(async () => { 
  await DbConnect(); 
  user = new User(MockUser);
  user.save();

  admin = new User(MockAdmin);
  admin.save();
});
afterAll(async () => await DbDisconnect());


/**
 * Filtering Test
 */
describe('Admin Middleware: Filtering Test', () => {
  afterEach(() => mockClear());

  test('should error if user not has user role', async () => {
    const req = getMockReq({
      connection: { remoteAddress: '127.0.0.1' },
      headers: { 'user-agent': UaString1 },
      redis: RdClient(),
      user: user
    });

    await userMiddleware(req, res, next);
    expect(res.status).toHaveBeenCalledWith(401);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: 'You don\'t have authorize for user section' });
  });

  test('should successfully if user has user role', async () => {
    const req = getMockReq({
      connection: { remoteAddress: '127.0.0.1' },
      headers: { 'user-agent': UaString1 },
      redis: RdClient(),
      user: admin
    });

    await userMiddleware(req, res, next);
    expect(next).toHaveBeenCalled();
  });
});
