import dotenv from 'dotenv'; dotenv.config();
import { getMockReq, getMockRes } from '@jest-mock/express';
import { User, UserDocument } from '../../src/models/user.model';
import { Token } from '../../src/models/auth.model';
import { DbConnect, DbDisconnect } from '../configs/mongodb.config';
import { RdClient } from '../configs/redis.config';
import authMiddleware from '../../src/middlewares/auth.middleware';
import tokenService from '../../src/services/token.service';
import userAgentService from '../../src/services/userAgent.service';
import authController from '../../src/controllers/auth.controller';
import { UaString1 } from '../mocks/userAgent.mock';
import { MockUser } from '../mocks/user.mock';


const { res, next, mockClear } = getMockRes();
const redis = RdClient();
let token: Token;
let user: UserDocument;
let clientIp: string;
let browserName: string;
beforeAll(async () => {
  clientIp = '127.0.0.1';

  await DbConnect();
  user = new User(MockUser);
  await user.save();

  const req = getMockReq({
    connection: { remoteAddress: clientIp },
    headers: { 'user-agent': UaString1 },
    redis: redis,
    body: {
      username: MockUser.username,
      password: MockUser.password
    }
  });
  
  await authController.Login(req, res);

  const userAgent = userAgentService.Parser(UaString1);
  browserName = userAgent.browser.name as string;
  token = await tokenService.Find(redis, clientIp, browserName, user.id);
});
afterAll(async () => await DbDisconnect());


/**
 * Filtering Test
 */
describe('Auth Middleware: Filtering Test', () => {
  beforeAll(() => mockClear());
  afterEach(() => mockClear());

  test('should error if not authen', async () => {
    const req = getMockReq({
      connection: { remoteAddress: clientIp },
      headers: { 'user-agent': UaString1 },
      redis: redis,
      user: user
    });

    await authMiddleware(req, res, next);
    expect(res.status).toHaveBeenCalledWith(401);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: 'No auth token' });
  });

  test('should successfully if token not expire', async () => {
    const req = getMockReq({
      connection: { remoteAddress: clientIp },
      headers: { 'user-agent': UaString1, authorization: `Bearer ${token.accessToken}` },
      redis: redis,
      user: user
    });

    await authMiddleware(req, res, next);
    expect(next).toHaveBeenCalled();
  });

  test('should error if token expired', async () => {
    redis.del(`tokens:${clientIp}:${browserName.toLocaleLowerCase()}:${user.id}`);

    const req = getMockReq({
      connection: { remoteAddress: clientIp },
      headers: { 'user-agent': UaString1, authorization: `Bearer ${token.accessToken}` },
      redis: redis,
      user: user
    });

    await authMiddleware(req, res, next);
    expect(res.status).toHaveBeenCalledWith(401);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: 'Unauthorized, Access Token was expired' });
  });
});
