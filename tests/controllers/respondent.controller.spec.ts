import { Types } from 'mongoose';
import { getMockReq, getMockRes } from '@jest-mock/express';
import { Questionnaire } from '../../src/models/questionnaire.model';
import { Respondent, RespondentDocument } from '../../src/models/respondent.model';
import { AnswerRequest } from '../../src/models/answer.model';
import questionService from '../../src/services/question.service';
import respondentController from '../../src/controllers/respondent.controller';
import { DbConnect, DbDisconnect } from '../configs/mongodb.config';
import { MockCreateQuestionnaire } from '../mocks/questionnaire.mock';
import { MockCreateQuestions } from '../mocks/question.mock';
import { MockCreateAnswers, MockUpdateAnswers, FakeId } from '../mocks/answer.mock';


let id: string;
let questionnaireId: string;
const mockCreateAnswers: AnswerRequest[] = [];
const mockUpdateAnswers: AnswerRequest[] = [];
const { res, mockClear } = getMockRes();
beforeAll(async () => { 
  await DbConnect();

  const questionnaire = new Questionnaire(MockCreateQuestionnaire);
  await questionnaire.save();
  questionnaireId = questionnaire.id;

  await Promise.all(MockCreateQuestions.map(async (payload, index) => {
    const question = await questionService.Create(questionnaireId, payload);
    
    mockCreateAnswers.push({
      question: question.id,
      ...MockCreateAnswers[index]
    });

    mockUpdateAnswers.push({
      question: question.id,
      ...MockUpdateAnswers[index]
    });
  }));
});
afterAll(async () => { await Questionnaire.deleteOne({ _id: Types.ObjectId(questionnaireId) }); await DbDisconnect(); });


/**
 * Creating Test
 */
describe('Respondent Controller: Creating Test', () => {
  afterEach(() => mockClear());
  afterAll(async () => {
    const respondents = await Respondent.find({}) as RespondentDocument[];
    id = respondents[0].id;
  });

  test('should successfully with 201 and return respondent', async () => {
    const req = getMockReq({
      params: { questionnaireId: questionnaireId },
      body: { answers: mockCreateAnswers }
    });

    await respondentController.Create(req, res);
    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledTimes(1);
  });
});


/**
 * Getting Test
 */
describe('Respondent Controller: Getting Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return respondents', async () => {
    const req = getMockReq({
      params: { questionnaireId: questionnaireId }
    });

    await respondentController.Fetch(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });
});


/**
 * Finding Test
 */
describe('Respondent Controller: Finding Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return respondent', async () => {
    const req = getMockReq({
      params: { id: id }
    });

    await respondentController.FindById(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });
});


/**
 * Updating Test
 */
describe('Respondent Controller: Updating Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return respondent', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: id
      },
      body: { answers: mockCreateAnswers }
    });

    await respondentController.Update(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });
});


/**
 * Deleting Test
 */
 describe('Respondent Controller: Deleting Test', () => {
  afterEach(() => mockClear());
  
  test('should successfully with 204', async () => {
    const req = getMockReq({
      params: { id: id }
    });

    await respondentController.Delete(req, res);
    expect(res.status).toHaveBeenCalledWith(204);
    expect(res.json).toHaveBeenCalledTimes(0);
  });
});