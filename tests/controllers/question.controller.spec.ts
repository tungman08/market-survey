import { Types } from 'mongoose';
import { getMockReq, getMockRes } from '@jest-mock/express';
import { Questionnaire } from '../../src/models/questionnaire.model';
import { DbConnect, DbDisconnect } from '../configs/mongodb.config';
import { User } from '../../src/models/user.model';
import { MockUser } from '../mocks/user.mock';
import questionController from '../../src/controllers/question.controller';
import { Question, QuestionType } from '../../src/models/question.model';
import { MockCreateQuestionnaire } from '../mocks/questionnaire.mock';
import { MockCreateQuestions, MockUpdateQuestions, FakeId } from '../mocks/question.mock';


let questionnaireId: string;
const idArray: string[] = [];
const { res, mockClear } = getMockRes();
beforeAll(async () => { 
  await DbConnect(); 
  const user = new User(MockUser);
  user.save();
  
  const questionnaire = new Questionnaire({ user: user.id, ...MockCreateQuestionnaire }); 
  await questionnaire.save(); 
  questionnaireId = questionnaire.id; 
});
afterAll(async () => { await Questionnaire.deleteOne({ _id: Types.ObjectId(questionnaireId) }); await DbDisconnect(); });


/**
 * Creating Test
 */
 describe('Question Controller: Creating Test', () => {
  afterAll(async () => {
    const questions = await Question.find({ questionnaire: questionnaireId }, null, { sort: { score: 1 } });
    questions.forEach((question) => idArray.push(question.id));
  });
  afterEach(() => mockClear());

  test('should successfully with 201 and return text question if create text question', async () => {
    const req = getMockReq({
      params: { questionnaireId: questionnaireId },
      body: MockCreateQuestions[0]
    });

    await questionController.Create(req, res);
    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should successfully with 201 and return option question if create option question', async () => {
    const req = getMockReq({
      params: { questionnaireId: questionnaireId },
      body: MockCreateQuestions[1]
    });

    await questionController.Create(req, res);
    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should successfully with 201 and return rating question if create rating question', async () => {
    const req = getMockReq({
      params: { questionnaireId: questionnaireId },
      body: MockCreateQuestions[2]
    });

    await questionController.Create(req, res);
    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should successfully with 201 and return option grid question if create option grid question', async () => {
    const req = getMockReq({
      params: { questionnaireId: questionnaireId },
      body: MockCreateQuestions[3]
    });

    await questionController.Create(req, res);
    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should successfully with 201 and return datetime question if create datetime question', async () => {
    const req = getMockReq({
      params: { questionnaireId: questionnaireId },
      body: MockCreateQuestions[4]
    });

    await questionController.Create(req, res);
    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should error with 422 if require field are empty', async () => {
    const req = getMockReq({
      params: { questionnaireId: questionnaireId },
      body: { required: true, questionType: QuestionType.TextBox }
    });

    await questionController.Create(req, res);
    expect(res.status).toHaveBeenCalledWith(422);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: '"questionBody" is required' });
  });

  test('should error with 422 if questionType is TEXT but create with OPTION', async () => {
    const fakeConfuse = { ...MockCreateQuestions[1] };
    fakeConfuse.questionType = QuestionType.TextBox;

    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId 
      },
      body: fakeConfuse
    });

    await questionController.Create(req, res);
    expect(res.status).toHaveBeenCalledWith(422);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: '"value" is invalid because "questionBody.name" failed to Equal QuestionType' });
  });

  test('should error with 422 if create unmatch type', async () => {
    const req = getMockReq({
      params: { questionnaireId: questionnaireId },
      body: { text: 'hello wolrd', lorem: 'lorem lorem lorem lorem' }
    });

    await questionController.Create(req, res);
    expect(res.status).toHaveBeenCalledWith(422);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: '"required" is required' });
  });
});


/**
 * Getting Test
 */
describe('Question Controller: Getting Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return all question in questionnaire', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId 
      }
    });

    await questionController.Fetch(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });
});


/**
 * Finding Test
 */
describe('Question Controller: Finding Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return textbox question', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[0]
      }
    });

    await questionController.FindById(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should successfully with 200 and return option question', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[1]
      }
    });

    await questionController.FindById(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });
  
  test('should successfully with 200 and return rating question', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[2]
      }
    });

    await questionController.FindById(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });
  
  test('should successfully with 200 and return option-grid question', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[3]
      }
    });

    await questionController.FindById(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });
  
  test('should successfully with 200 and return datetime question', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[4]
      }
    });

    await questionController.FindById(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should error with 404 if id is empty', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: FakeId
      }
    });

    await questionController.FindById(req, res);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: 'Not found' });
  });
});


/**
 * Updating Test
 */
describe('Questionnaire Controller: Updating Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return question if update textbox', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[0]
      },
      body: MockUpdateQuestions[0]
    });

    await questionController.Update(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should successfully with 200 and return question if update option', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[1]
      },
      body: MockUpdateQuestions[1]
    });

    await questionController.Update(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should successfully with 200 and return question if update rating', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[2]
      },
      body: MockUpdateQuestions[2]
    });

    await questionController.Update(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should successfully with 200 and return question if update option grid', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[3]
      },
      body: MockUpdateQuestions[3]
    });

    await questionController.Update(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should successfully with 200 and return question if update datetime', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[4]
      },
      body: MockUpdateQuestions[4]
    });

    await questionController.Update(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should error with 404 if not found', async () => {
    const fakeConfuse = { ...MockCreateQuestions[1] };
    fakeConfuse.questionType = QuestionType.TextBox;

    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: FakeId
      },
      body: MockUpdateQuestions[4]
    });

    await questionController.Update(req, res);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: 'Not found' });
  });

  test('should error with 422 if questionType is TEXT but update with OPTION', async () => {
    const fakeConfuse = { ...MockCreateQuestions[1] };
    fakeConfuse.questionType = QuestionType.TextBox;

    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[0]
      },
      body: fakeConfuse
    });

    await questionController.Update(req, res);
    expect(res.status).toHaveBeenCalledWith(422);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: '"value" is invalid because "questionBody.name" failed to Equal QuestionType' });
  });

  test('should error with 422 if update unmatch type', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[0]
      },
      body: { text: 'hello wolrd', test: 'lorem' }
    });

    await questionController.Update(req, res);
    expect(res.status).toHaveBeenCalledWith(422);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: '"required" is required' });
  });
});


/**
 * Move Up Test
 */
describe('Question Controller: Move Test', () => {
  afterEach(() => mockClear());

  test('should error with 400 if cannot move up', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[0]
      }
    });

    await questionController.MoveUp(req, res);
    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: 'Cannot move' });
  });

  test('should error with 400 if cannot move down', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[4]
      }
    });

    await questionController.MoveDown(req, res);
    expect(res.status).toHaveBeenCalledWith(400);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: 'Cannot move' });
  });

  test('should successfully with 200 if move up', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[1]
      }
    });

    await questionController.MoveUp(req, res);
    expect(res.status).toHaveBeenCalledWith(204);
    expect(res.json).toHaveBeenCalledTimes(0);
  });

  test('should successfully with 200 if move down', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[3]
      }
    });

    await questionController.MoveDown(req, res);
    expect(res.status).toHaveBeenCalledWith(204);
    expect(res.json).toHaveBeenCalledTimes(0);
  });
});


/**
 * Deleting Test
 */
describe('Question Controller: Deleting Test', () => {
  afterEach(() => mockClear());
  
  test('should successfully with 204 if delete text question', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[0]
      }
    });

    await questionController.Delete(req, res);
    expect(res.status).toHaveBeenCalledWith(204);
    expect(res.json).toHaveBeenCalledTimes(0);
  });

  test('should successfully with 204 if delete option question', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[0]
      }
    });

    await questionController.Delete(req, res);
    expect(res.status).toHaveBeenCalledWith(204);
    expect(res.json).toHaveBeenCalledTimes(0);
  });

  test('should successfully with 204 if delete rating question', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[2]
      }
    });

    await questionController.Delete(req, res);
    expect(res.status).toHaveBeenCalledWith(204);
    expect(res.json).toHaveBeenCalledTimes(0);
  });

  test('should successfully with 204 if delete option grid question', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[3]
      }
    });

    await questionController.Delete(req, res);
    expect(res.status).toHaveBeenCalledWith(204);
    expect(res.json).toHaveBeenCalledTimes(0);
  });

  test('should successfully with 204 if delete datetime question', async () => {
    const req = getMockReq({
      params: { 
        questionnaireId: questionnaireId,
        id: idArray[4]
      }
    });

    await questionController.Delete(req, res);
    expect(res.status).toHaveBeenCalledWith(204);
    expect(res.json).toHaveBeenCalledTimes(0);
  });
});
