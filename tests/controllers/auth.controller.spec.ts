import dotenv from 'dotenv'; dotenv.config();
import { getMockReq, getMockRes } from '@jest-mock/express';
import { User, UserDocument } from '../../src/models/user.model';
import authController from '../../src/controllers/auth.controller';
import { DbConnect, DbDisconnect } from '../configs/mongodb.config';
import { RdClient } from '../configs/redis.config';
import { UaString1 } from '../mocks/userAgent.mock';
import { MockUser, MockUpdateUser, MockChangePassword } from '../mocks/user.mock';


let user: UserDocument;
const { res, mockClear } = getMockRes();
beforeAll(async () => { await DbConnect(); user = new User(MockUser); await user.save(); });
afterAll(async () => await DbDisconnect());


/**
 * Login Test
 */
describe('Auth Controller: Login Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return token', async () => {
    const req = getMockReq({
      connection: { remoteAddress: '127.0.0.1' },
      headers: { 'user-agent': UaString1 },
      redis: RdClient(),
      body: {
        username: MockUser.username,
        password: MockUser.password
      }
    });
    
    await authController.Login(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should error with 401 if invalid credentials', async () => {
    const req = getMockReq({
      connection: { remoteAddress: '127.0.0.1' },
      headers: { 'user-agent': UaString1 },
      redis: RdClient(),
      body: {
        username: 'invalid',
        password: 'invalid'
      }
    });

    await authController.Login(req, res);
    expect(res.status).toHaveBeenCalledWith(401);
    expect(res.json).toHaveBeenCalledWith({ message: 'Incorrect username or password' });
  });

  test('should error with 401 if empty credentialsuse empty credentials', async () => {
    const req = getMockReq({
      connection: { remoteAddress: '127.0.0.1' },
      headers: { 'user-agent': UaString1 },
      redis: RdClient(),
      body: {
        username: MockUser.username
      }
    });

    await authController.Login(req, res);
    expect(res.status).toHaveBeenCalledWith(422);
    expect(res.json).toHaveBeenCalledWith({ message: '"password" is required' });
  });
});


/**
 * Logout Test
 */
describe('Auth Controller: Logout Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 204', async () => {
    const req = getMockReq({
      connection: { remoteAddress: '127.0.0.1' },
      headers: { 'user-agent': UaString1 },
      redis: RdClient(),
      user: user
    });

    await authController.Logout(req, res);
    expect(res.status).toHaveBeenCalledWith(204);
  });
});


/**
 * Profile Test
 */
describe('Auth Controller: Profile Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return profile', () => {
    const req = getMockReq({
      user: user
    });

    authController.Profile(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });
});


/**
 * Update Profile Test
 */
describe('Auth Controller: Update Profile Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and update profile', async () => {
    const req = getMockReq({
      user: user,
      body: {
        firstName: MockUpdateUser.firstName,
        lastName: MockUpdateUser.lastName,
        email: MockUpdateUser.email
      }
    });

    await authController.UpdateProfile(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should error with 422 if invalid field', async () => {
    const req = getMockReq({
      user: user,
      body: {}
    });

    await authController.UpdateProfile(req, res);
    expect(res.status).toHaveBeenCalledWith(422);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: '"firstName" is required' });
  });
});


/**
 * Change Password Test
 */
describe('Auth Controller: Change Password Test', () => {
  afterEach(() => mockClear());

  test('should error with 422 and password not changed if invalid old password', async () => {
    const req = getMockReq({
      user: user,
      body: {
        oldPassword: 'invalid',
        password: MockChangePassword.password
      }
    });

    await authController.ChangePassword(req, res);
    expect(res.status).toHaveBeenCalledWith(422);
    expect(res.json).toHaveBeenCalledWith({ message: 'Password incorrect' });
  });

  test('should successfully with 200 and password changed', async () => {
    const req = getMockReq({
      user: user,
      body: {
        oldPassword: MockUser.password,
        password: MockChangePassword.password
      }
    });

    await authController.ChangePassword(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith(
      expect.objectContaining({
        passwordChanged: true
      })
    );
  });
});
