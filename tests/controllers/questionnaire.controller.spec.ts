import { getMockReq, getMockRes } from '@jest-mock/express';
import { Questionnaire, QuestionnaireDocument } from '../../src/models/questionnaire.model';
import { DbConnect, DbDisconnect } from '../configs/mongodb.config';
import questionnaireController from '../../src/controllers/questionnaire.controller';
import { User, UserDocument } from '../../src/models/user.model';
import { MockUser } from '../mocks/user.mock';
import { MockCreateQuestionnaire, MockUpdateQuestionnaire, FakeId } from '../mocks/questionnaire.mock';


let id: string;
let user: UserDocument;
const { res, mockClear } = getMockRes();
beforeAll(async () => { await DbConnect(); user = new User(MockUser); await user.save(); });
afterAll(async () => await DbDisconnect());


/**
 * Creating Test
 */
describe('Questionnaire Controller: Creating Test', () => {
  afterEach(() => mockClear());
  afterAll(async () => {
    const questionnaires = await Questionnaire.find({}) as QuestionnaireDocument[];
    id = questionnaires[0].id;
  });

  test('should successfully with 201 and return questionnaire', async () => {
    const req = getMockReq({
      user: user,
      body: MockCreateQuestionnaire
    });

    await questionnaireController.Create(req, res);
    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should error with 422 if invalid field', async () => {
    const req = getMockReq({
      user: user,
      body: { title: 'dummy_name' }
    });

    await questionnaireController.Create(req, res);
    expect(res.status).toHaveBeenCalledWith(422);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: '"description" is required' });
  });
});


/**
 * Getting Test
 */
describe('Questionnaire Controller: Getting Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return all questionnaires', async () => {
    const req = getMockReq({
      user: user
    });

    await questionnaireController.Fetch(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });
});


/**
 * Finding Test
 */
describe('Questionnaire Controller: Finding Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return questionnaire', async () => {
    const req = getMockReq({
      params: { id: id }
    });

    await questionnaireController.FindById(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should error with 404 if not found', async () => {
    const req = getMockReq({
      params: { id: FakeId }
    });

    await questionnaireController.FindById(req, res);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: 'Not found' });
  });
});


/**
 * Getting Result Test
 */
 describe('Questionnaire Controller: Getting Result Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return questionnaire result', async () => {
    const req = getMockReq({
      params: { id: id }
    });

    await questionnaireController.GetResult(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith(
      expect.objectContaining({
        questions: [],
        respondents: []
      })
    );
  });

  test('should error with 404 if not found', async () => {
    const req = getMockReq({
      params: { id: FakeId }
    });

    await questionnaireController.GetResult(req, res);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: 'Not found' });
  });
});


/**
 * Getting Form Test
 */
 describe('Questionnaire Controller: Getting Form Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return questionnaire form', async () => {
    const req = getMockReq({
      params: { id: id }
    });

    await questionnaireController.GetForm(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith(
      expect.objectContaining({
        questions: []
      })
    );
  });

  test('should error with 404 if not found', async () => {
    const req = getMockReq({
      params: { id: FakeId }
    });

    await questionnaireController.GetResult(req, res);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: 'Not found' });
  });
});


/**
 * Updating Test
 */
describe('Questionnaire Controller: Updating Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return questionnaire', async () => {
    const req = getMockReq({
      params: { id: id },
      body: MockUpdateQuestionnaire
    });

    await questionnaireController.Update(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should error with 404 if not found', async () => {
    const req = getMockReq({
      params: { id: FakeId },
      body: MockUpdateQuestionnaire
    });

    await questionnaireController.Update(req, res);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: 'Not found' });
  });

  test('should error with 422 if invalid field', async () => {
    const req = getMockReq({
      params: { id: id },
      body: { title: 'dummy_name' }
    });

    await questionnaireController.Update(req, res);
    expect(res.status).toHaveBeenCalledWith(422);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: '"description" is required' });
  });
});


/**
 * Deleting Test
 */
describe('Questionnaire Controller: Deleting Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 204', async () => {
    const req = getMockReq({
      params: { id: FakeId }
    });

    await questionnaireController.Delete(req, res);
    expect(res.status).toHaveBeenCalledWith(204);
    expect(res.json).toHaveBeenCalledTimes(0);
  });
});
