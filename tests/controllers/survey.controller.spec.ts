import { Types } from 'mongoose';
import { getMockReq, getMockRes } from '@jest-mock/express';
import { Questionnaire } from '../../src/models/questionnaire.model';
import { Respondent, RespondentDocument } from '../../src/models/respondent.model';
import { AnswerRequest } from '../../src/models/answer.model';
import questionService from '../../src/services/question.service';
import surveyController from '../../src/controllers/survey.controller';
import { DbConnect, DbDisconnect } from '../configs/mongodb.config';
import { MockCreateQuestionnaire } from '../mocks/questionnaire.mock';
import { MockCreateQuestions } from '../mocks/question.mock';
import { MockCreateAnswers, MockUpdateAnswers, FakeId } from '../mocks/answer.mock';

let id: string;
const mockCreateAnswers: AnswerRequest[] = [];
const { res, mockClear } = getMockRes();
beforeAll(async () => { 
  await DbConnect();

  const questionnaire = new Questionnaire(MockCreateQuestionnaire);
  await questionnaire.save();
  id = questionnaire.id;

  await Promise.all(MockCreateQuestions.map(async (payload, index) => {
    const question = await questionService.Create(id, payload);
    
    mockCreateAnswers.push({
      question: question.id,
      ...MockCreateAnswers[index]
    });
  }));
});
afterAll(async () => { await Questionnaire.deleteOne({ _id: Types.ObjectId(id) }); await DbDisconnect(); });


/**
 * Getting Form Test
 */
describe('Questionnaire Controller: Getting Form Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return questionnaire form', async () => {
    const req = getMockReq({
      params: { id: id }
    });

    await surveyController.GetForm(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith(
      expect.objectContaining({
        questions: expect.any(Array)
      })
    );
  });
});


/**
 * Creating Test
 */
describe('Respondent Controller: Creating Test', () => {
  afterEach(() => mockClear());
  afterAll(async () => {
    const respondents = await Respondent.find({}) as RespondentDocument[];
    id = respondents[0].id;
  });

  test('should successfully with 201 and return respondent', async () => {
    const req = getMockReq({
      params: { id: id },
      body: { answers: mockCreateAnswers }
    });

    await surveyController.Create(req, res);
    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledTimes(1);
  });
});