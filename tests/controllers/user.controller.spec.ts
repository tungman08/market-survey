import dotenv from 'dotenv'; dotenv.config();
import { getMockReq, getMockRes } from '@jest-mock/express';
import { User, UserDocument } from '../../src/models/user.model';
import { DbConnect, DbDisconnect } from '../configs/mongodb.config';
import userController from '../../src/controllers/user.controller';
import { MockCreateUser, MockInvalideUser, MockUpdateUser,
  MockSecretAdmin, MockUserChangePassword, MockUserInvalidChangePassword, FakeId } from '../mocks/user.mock';


let id: string;
const { res, mockClear } = getMockRes();
beforeAll(async () => { await DbConnect(); const secretAdmin = new User(MockSecretAdmin); secretAdmin.save(); });
afterAll(async () => await DbDisconnect());


/**
 * Creating Test
 */
describe('User Controller: Creating Test', () => {
  afterEach(() => mockClear());
  afterAll(async () => {
    const user = await User.findOne({ username: MockCreateUser.username }) as UserDocument;
    id = user.id;
  });

  test('should successfully with 201 and return user', async () => {
    const req = getMockReq({
      body: MockCreateUser
    });

    await userController.Create(req, res);
    expect(res.status).toHaveBeenCalledWith(201);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should error with 422 if invalid field', async () => {
    const req = getMockReq({
      body: MockInvalideUser
    });

    await userController.Create(req, res);
    expect(res.status).toHaveBeenCalledWith(422);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      message: '"email" is required'
    });
  });

  test('should error with 500 if create duplicate user', async () => {
    const req = getMockReq({
      body: MockCreateUser
    });

    await userController.Create(req, res);
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ 
      message: 'E11000 duplicate key error dup key: { : "username_1" }' 
    });
  });
});


/**
 * Getting Test
 */
 describe('User Controller: Getting Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return all users', async () => {
    const req = getMockReq();

    await userController.Fetch(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });
});


/**
 * Finding Test
 */
describe('User Controller: Finding Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return user', async () => {
    const req = getMockReq({
      params: { id: id }
    });

    await userController.FindById(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should error with 404 if not found', async () => {
    const req = getMockReq({
      params: { id: FakeId }
    });

    await userController.FindById(req, res);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({ message: 'Not found' });
  });
});


/**
 * Updating Test
 */
describe('User Controller: Updating Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return user', async () => {
    const req = getMockReq({
      params: { id: id },
      body: MockUpdateUser
    });

    await userController.Update(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
  });

  test('should error with 404 if user not found', async () => {
    const req = getMockReq({
      params: { id: FakeId },
      body: MockUpdateUser
    });

    await userController.Update(req, res);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      message: 'Not found'
    });
  });

  test('should error with 422 if invalid field', async () => {
    const req = getMockReq({
      params: { id: id },
      body: MockInvalideUser
    });

    await userController.Update(req, res);
    expect(res.status).toHaveBeenCalledWith(422);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      message: '"email" is required'
    });
  });  
});


/**
 * Changing Password Test
 */
 describe('User Controller: Changing Password Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 200 and return user', async () => {
    const req = getMockReq({
      params: { id: id },
      body: MockUserChangePassword
    });

    await userController.ResetPassword(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith(
      expect.objectContaining({
        passwordChanged: false
      })
    );
  });

  test('should error with 404 if user not found', async () => {
    const req = getMockReq({
      params: { id: FakeId },
      body: MockUserChangePassword
    });

    await userController.ResetPassword(req, res);
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      message: 'Not found'
    });
  });
  
  test('should error with 422 if invalid secret password', async () => {
    const req = getMockReq({
      params: { id: id },
      body: MockUserInvalidChangePassword
    });

    await userController.ResetPassword(req, res);
    expect(res.status).toHaveBeenCalledWith(422);
    expect(res.json).toHaveBeenCalledTimes(1);
    expect(res.json).toHaveBeenCalledWith({
      message: 'Secret admin password incorrect'
    });
  });
});


/**
 *  Deleting Test
 */
describe('User Controller: Deleting Test', () => {
  afterEach(() => mockClear());

  test('should successfully with 204', async () => {
    const req = getMockReq({
      params: { id: id }
    });

    await userController.Delete(req, res);
    expect(res.status).toHaveBeenCalledWith(204);
    expect(res.json).toHaveBeenCalledTimes(0);
  });
});
