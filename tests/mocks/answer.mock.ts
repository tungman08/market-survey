import { AnswerRequest } from '../../src/models/answer.model';

export const MockCreateAnswers: AnswerRequest[] = [
  {
    value: 'textbox_answer'
  },
  {
    value: [1, 2, 3, 4, 5]
  },
  {
    value: 5
  },
  {
    value: [
      [1, 1, 1, 1, 1],
      [2, 2, 2, 2, 2],
      [3, 3, 3, 3, 3],
      [4, 4, 4, 4, 4],
      [5, 5, 5, 5, 5]
    ]
  },
  {
    value: new Date()
  }
];

export const MockUpdateAnswers: AnswerRequest[] = [
  {
    value: 'textbox_answer_update'
  },
  {
    value: [2, 4, 5, 1, 3]
  },
  {
    value: 5
  },
  {
    value: [
      [5, 4, 3, 2, 1],
      [5, 4, 3, 2, 1],
      [5, 4, 3, 2, 1],
      [5, 4, 3, 2, 1],
      [5, 4, 3, 2, 1]
    ]
  },
  {
    value: new Date(2021, 9, 9)
  }
];

export const FakeQuestionnaireId: string = '607149408180832500e0cd3d';
export const FakeQuestionId: string = '607149408180832500e0cd3d';
export const FakeId: string = '607149408180832500e0cd3d';