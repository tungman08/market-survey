import { QuestionnaireRequest } from '../../src/models/questionnaire.model';

export const MockCreateQuestionnaire: QuestionnaireRequest = {
  title: 'dummy_name',
  description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
  releaseAt: new Date(),
  dueAt: new Date(),
};

export const MockUpdateQuestionnaire: QuestionnaireRequest = {
  title: 'updated_name',
  description:'hello world Lorem ipsum dolor sit amet, consectetur adipiscing elit,',
  releaseAt: new Date(2021, 9, 9),
  dueAt: new Date(2021, 9, 9)
};

export const FakeId: string = '607149408180832500e0cd3d';
