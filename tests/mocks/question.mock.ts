import { QuestionType, QuestionRequest, QuestionOptionType, QuestionRatingSymbol } from '../../src/models/question.model';

export const MockCreateQuestions: QuestionRequest[] = [
  {
    required: true,
    questionType: QuestionType.TextBox,
    questionBody: {
      name:  QuestionType.TextBox,
      text: 'textbox_question',
      isTextArea: false
    }
  },
  {
    required: true,
    questionType: QuestionType.Choice,
    questionBody: {
      name:  QuestionType.Choice,
      text: 'option_question',
      optionType: QuestionOptionType.DropDownList,
      options: ['option1', 'option2', 'option3'],
      hasOther: false
    }
  },
  {
    required: true,
    questionType: QuestionType.Rating,
    questionBody: {
      name:  QuestionType.Rating,
      text: 'rating_question',
      level: 5,
      symbol: QuestionRatingSymbol.Star,
      label: { show: false, start: '', end: '' }
    }
  },
  {
    required: true,
    questionType: QuestionType.Likert,
    questionBody: {
      name:  QuestionType.Likert,
      text: 'likert_question',
      isCheckBox: false,
      rows: ['question1', 'question2', 'question3'],
      columns: ['option1', 'option2', 'option3']
    }
  },
  {
    required: true,
    questionType: QuestionType.DateTime,
    questionBody: {
      name:  QuestionType.DateTime,
      text: 'date_question',
      isTime: false
    }
  }
];

export const MockUpdateQuestions: QuestionRequest[] = [
  {
    required: true,
    questionType: QuestionType.TextBox,
    questionBody: {
      name: QuestionType.TextBox,
      text: 'textbox_question_updated',
      isTextArea: true
    }
  },
  {
    required: true,
    questionType: QuestionType.Choice,
    questionBody: {
      name: QuestionType.Choice,
      text: 'option_question_updated',
      optionType: QuestionOptionType.RadioBox,
      options: ['option1', 'option2', 'option3', 'option4'],
      hasOther: false
    }
  },
  {
    required: true,
    questionType: QuestionType.Rating,
    questionBody: {
      name: QuestionType.Rating,
      text: 'rating_question_updated',
      level: 10,
      symbol: QuestionRatingSymbol.Number,
      label: { show: true, start: 'start', end: 'end' }
    }
  },
  {
    required: true,
    questionType: QuestionType.Likert,
    questionBody: {
      name: QuestionType.Likert,
      text: 'option_grid_question_updated',
      isCheckBox: false,
      rows: ['question1', 'question2'],
      columns: ['option1', 'option2']
    }
  },
  {
    required: true,
    questionType: QuestionType.DateTime,
    questionBody: {
      name: QuestionType.DateTime,
      text: 'date_question_updated',
      isTime: true
    }
  }
];

export const MockInvalidQuestion = {
  questionBody: {
    text: 'dummy_text_type',
    isTextArea: false
  }
};

export const FakeQuestionnaireId: string = '607149408180832500e0cd3d';
export const FakeId: string = '607149408180832500e0cd3d';
