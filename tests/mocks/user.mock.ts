import { Role, UserRequest,  } from '../../src/models/user.model';

export const MockUser = {
  username: 'username_1',
  password: 'password_1',
  firstName: 'firstName_1',
  lastName: 'lastName_1',
  email: 'dummy_1@email.com',
  roles: [ Role.User ],
  active: true,
  passwordChanged: false
};

export const MockAdmin = {
  username: 'admin_1',
  password: 'password_1',
  firstName: 'admin_1',
  lastName: 'admin_1',
  email: 'admin_1@email.com',
  roles: [ Role.Admin, Role.User ],
  active: true,
  passwordChanged: false
};

export const MockCreateUser: UserRequest = {
  username: 'username_1',
  password: 'password_1',
  firstName: 'firstName_1',
  lastName: 'lastName_1',
  email: 'dummy_1@email.com',
  roles: [ Role.Admin, Role.User ]
};

export const MockUpdateUser: UserRequest = {
  firstName: 'firstName_2',
  lastName: 'lastName_2',
  email: 'dummy_2@email.com',
  active: true,
  roles: [ Role.User ]
};

export const MockInvalideUser = {
  username: 'username_1',
  password: 'password_1',
  firstName: 'firstName_2',
  lastName: 'lastName_2',
  roles: []
};

export const MockChangePassword = {
  password: 'password_changed'
};

export const MockSecretAdmin = {
  username: 'secret',
  password: 'secret_password',
  firstName: 'secret',
  lastName: 'secret',
  email: 'secret@email.com',
  roles: [ Role.SecretAdmin, Role.Admin ],
  active: true,
  passwordChanged: false
};

export const MockUserChangePassword = {
  password: 'password_changed',
  secretAdmin: 'secret',
  secretPassword: 'secret_password'
};

export const MockUserInvalidChangePassword = {
  password: 'password_changed',
  secretAdmin: 'secret',
  secretPassword: 'secret_invalid_password'
};

export const FakeId: string = '607149408180832500e0cd3d';
export const FakeUserName: string = "dummy";
