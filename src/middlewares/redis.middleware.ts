import { Request, Response, NextFunction } from 'express';
import { RdClient } from '../configs/redis.config';

const middleware = async (req: Request, _: Response, next: NextFunction): Promise<void> => {
  req.redis = RdClient();
  next();
};

export default middleware;
