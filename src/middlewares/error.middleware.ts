import { Request, Response, NextFunction } from 'express';
import statusCode from 'http-status-codes';

const middleware = (err: Error, req: Request, res: Response, next: NextFunction): any => {
  if (req.xhr) {
    return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: err.message });
  } else {
    next(err);
  }
};

export default middleware;
