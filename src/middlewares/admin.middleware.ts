import { Request, Response, NextFunction } from 'express';
import statusCode from 'http-status-codes';
import userService from '../services/user.service';
import { Role, UserDocument } from '../models/user.model';

const middleware = async (req: Request, res: Response, next: NextFunction): Promise<any> => {
  const { id } = req.user as UserDocument;

  try {
    const user = await userService.FindById(id) as UserDocument;

    if (!user || !user.roles.includes(Role.Admin)) {
      return res.status(statusCode.UNAUTHORIZED).json({ message: 'You don\'t have authorize for admin section' });
    }
  
    next();
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  } 
};

export default middleware;
