import { Request, Response, NextFunction } from 'express';
import requestIp from 'request-ip';
import { RdClient } from '../configs/redis.config';
import authen from '../configs/passport.config';
import statusCode from 'http-status-codes';
import { UserDocument } from '../models/user.model';
import { AuthResult } from '../models/auth.model';
import tokenService from '../services/token.service';
import userAgentService from '../services/userAgent.service';

const middleware = async (req: Request, res: Response, next: NextFunction): Promise<any> => {
  try {
    const result = await authenticate(req, res, next);
    req.user = result.body;
    next();
  } catch (error: unknown) {
    const err = error as AuthResult;
    return res.status(err.statusCode).json({ message: err.body.message });
  }
};

const authenticate = (req: Request, res: Response, next: NextFunction) => {
  return new Promise<AuthResult>((resolve, reject) => {
    authen.authenticate('jwt', { session: false }, async (error: Error, user: UserDocument, info: { message: string }) => {
      if (error) {
        reject({
          statusCode: statusCode.INTERNAL_SERVER_ERROR,
          body: { message: error.message }
        });
      } else if (!user) {
        reject({
          statusCode: statusCode.UNAUTHORIZED,
          body: { message: info.message }
        });
      } else {
        const redis = req.redis ?? RdClient();
        const clientIp = requestIp.getClientIp(req) as string;
        const userId = user.id as string;
        const browserName = getBrowserName(req.headers['user-agent']);
        const token = await tokenService.Find(redis, clientIp, browserName, userId);
        
        if (token.accessToken) {
          resolve({ 
            statusCode: statusCode.OK,
            body: user
          });
        } else {
          reject({
            statusCode: statusCode.UNAUTHORIZED,
            body: { message: 'Unauthorized, Access Token was expired' }
          });
        }
      }
    })(req, res, next);
  });
};

const getBrowserName = (uaString: string | undefined) => {
  const userAgent = userAgentService.Parser(uaString as string);
  return userAgent.browser.name as string;
};

export default middleware;
