export default {
  type: 'object',
  properties: {
    message: {
      type: 'string',
      example: 'Not found'
    }
  }
};