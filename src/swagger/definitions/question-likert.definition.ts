export default {
  type: 'object',
  properties: {
    text: {
      description: 'ข้อคำถาม',
      type: 'string'
    },
    isCheckBox: {
      description: 'ประเภทตัวเลือก',
      type: 'boolean'
    },
    rows: {
      description: 'ตัววัด',
      type: 'array',
      items: {
        type: 'string'
      }
    },
    columns: {
      description: 'ตัวเลือก',
      type: 'array',
      items: {
        type: 'string'
      }
    }
  }
};
