export default {
  type: 'object',
  properties: {
    text: {
      description: 'ข้อคำถาม',
      type: 'string'
    },
    level: {
      description: 'ระดับคะแนน',
      type: 'number'
    },
    symbol: {
      description: 'สัญลักษณ์',
      type: 'string',
      enum: [
        'STAR',
        'NUMBER'
      ],
      default: 'TEXTBOX'
    },
    label: {
      description: 'ป้ายชื่อ',
      type: 'object',
      properties: {
        show: {
          description: 'แสดงผล',
          type: 'boolean'
        },
        start: {
          description: 'ข้อมูลเริ่มต้น',
          type: 'string'
        },
        end: {
          description: 'ข้อมูลสิ้นสุด',
          type: 'string'
        }
      }
    }
  }
};
