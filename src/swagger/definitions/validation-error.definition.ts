export default {
  type: 'object',
  properties: {
    message: {
      type: 'string',
      example: 'Invalid value'
    }
  }
};
