export default {
  type: 'object',
  properties: {
    message: {
      type: 'string',
      example: 'Internal server error'
    }
  }
};