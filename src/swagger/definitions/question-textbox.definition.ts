export default {
  type: 'object',
  properties: {
    text: {
      description: 'ข้อคำถาม',
      type: 'string'
    },
    isTextArea: {
      description: 'ใช้เป็น text area',
      type: 'boolean'
    }
  }
};
