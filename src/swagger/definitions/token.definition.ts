export default {
  type: 'object',
  properties: {
    accessToken: {
      description: 'โทเคน',
      type: 'string'
    },
    refreshToken: {
      description: 'รีเฟซโทเคน',
      type: 'string'
    },
    expiresIn: {
      description: 'อายุของโทเคน',
      type: 'number'
    },
    refreshExpiresIn: {
      description: 'อายุของรีเฟซโทเคน',
      type: 'number'
    }
  }
};
