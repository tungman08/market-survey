export default {
  type: 'object',
  properties: {
    id: {
      description: 'รหัสผู้ใช้งานระบบ',
      type: 'string'
    },
    username: {
      description: 'ชื่อผู้ใช้งานระบบ',
      type: 'string'
    },
    firstName: {
      description: 'ชื่อ',
      type: 'string'
    },
    lastName: {
      description: 'นามสกุล',
      type: 'string'
    },
    email: {
      description: 'อีเมล',
      type: 'string'
    },
    active: {
      description: 'สถานะ',
      type: 'boolean'
    },
    roles: {
      description: 'สิทธิ์การใช้งาน',
      type: 'array',
      items: {
        type: 'string',
        enum: [
          'ADMIN',
          'USER'
        ],
        default: 'USER'
      }
    },
    passwordChanged: {
      description: 'สถานะการเปลี่ยนรหัสผ่าน',
      type: 'boolean'
    }
  }
};
