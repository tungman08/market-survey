export default {
  type: 'object',
  properties: {
    id: {
      description: 'รหัสแบบข้อคำถาม',
      type: 'string'
    },
    required: {
      description: 'จำเป็นต้องกรอก',
      type: 'boolean'
    },
    questionType: {
      description: 'ประเภทข้อคำถาม',
      type: 'string',
      enum: [
        'TEXTBOX',
        'OPTION',
        'RATING',
        'OPTIONGRID',
        'DATETIME'
      ],
      default: 'TEXTBOX'
    },
    score: {
      description: 'คะแนนสำหรับจัดเรียงข้อมูล',
      type: 'number'
    },
    questionBody: {
      description: 'ข้อมูลข้อคำถาม เป็นได้ทั้ง QuestionTextBox, QuestionOption, QuestionRating, QuestionOptionGrid หรือ QuestionDateTime',
      type: 'object',
      properties: {
        text: {
          description: 'ข้อคำถาม',
          type: 'string'
        },
        isTextArea: {
          description: 'ใช้เป็น text area',
          type: 'boolean'
        }
      }
    },
  }
};
