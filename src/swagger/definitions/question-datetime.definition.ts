export default {
  type: 'object',
  properties: {
    text: {
      description: 'ข้อคำถาม',
      type: 'string'
    },
    isTime: {
      description: 'ประเภทตัวเลือก',
      type: 'boolean'
    }
  }
};
