export default {
  type: 'object',
  properties: {
    id: {
      description: 'รหัสแบบสอบถาม',
      type: 'string'
    },
    name: {
      description: 'ชื่อแบบสอบถาม',
      type: 'string'
    },
    intro: {
      description: 'บทนำ',
      type: 'string'
    },
    questions: {
      description: 'ข้อคำถาม เป็นได้ทั้ง TextBox, Option, Rating, Option Grid หรือ DateTime',
      type: 'array',
      items: {
        type: 'object',
        properties: {
          text: {
            description: 'ข้อคำถาม',
            type: 'string'
          },
          isTextArea: {
            description: 'ใช้เป็น text area',
            type: 'boolean'
          }
        }
      }
    },
  }
};