export default {
  type: 'object',
  properties: {
    id: {
      description: 'รหัสแบบสอบถาม',
      type: 'string'
    },
    title: {
      description: 'ชื่อแบบสอบถาม',
      type: 'string'
    },
    description: {
      description: 'บทนำ',
      type: 'string'
    },
    questions: {
      description: 'ข้อมูลคำถามทั้งหมด',
      type: 'array',
      items: {
        type: 'object',
        properties: {
          id: {
            description: 'รหัสคำถาม',
            type: 'string'
          },
          questionType: {
            description: 'ประเภทข้อคำถาม',
            type: 'string',
            enum: [
              'TEXTBOX',
              'OPTION',
              'RATING',
              'OPTIONGRID',
              'DATETIME'
            ],
            default: 'TEXT'
          },
          values: {
            description: 'ข้อมูลคำตอบ เป็นได้ทั้ง string[], number[][], number[], number[][][], datetime[]',
            type: 'array',
            items: {
              type: 'array',
              items: {
                type: 'string',
              }
            }
          }
        }
      }
    },
    answers: {
      description: 'ข้อมูลคำตอบทั้งหมดแยกตามข้อคำถาม',
      type: 'array',
      items: {
        type: 'object',
        properties: {
          questionId: {
            description: 'รหัสคำถาม',
            type: 'string'
          },
          values: {
            description: 'ข้อมูลคำตอบ เป็นได้ทั้ง string[], number[][], number[], number[][][], datetime[]',
            type: 'array',
            items: {
              type: 'array',
              items: {
                type: 'string',
              }
            }
          }
        }
      }
    }
  }
};