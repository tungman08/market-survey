export default {
  type: 'object',
  properties: {
    id: {
      description: 'รหัสแบบสอบถาม',
      type: 'string'
    },
    title: {
      description: 'ชื่อแบบสอบถาม',
      type: 'string'
    },
    description: {
      description: 'บทนำ',
      type: 'string'
    },
    releaseAt: {
      description: 'วันที่เผยแพร่',
      type: 'string',
      format: 'date'
    },
    dueAt: {
      description: 'วันที่ปิดแบบสอบถาม',
      type: 'string',
      format: 'date'
    },

  }
};
