export default {
  type: 'object',
  properties: {
    text: {
      description: 'ข้อคำถาม',
      type: 'string'
    },
    optionType: {
      description: 'ประเภทของตัวเลือก',
      type: 'string',
      enum: [
        'CHECKBOX',
        'RADIOBOX',
        'DROPDOWNLIST'
      ],
      default: 'CHECKBOX'
    },
    options: {
      description: 'ตัวเลือก',
      type: 'array',
      items: {
        type: 'string'
      }
    },
    hasOther: {
      description: 'มีตัวเลือกอื่นๆ',
      type: 'boolean'
    }
  }
};
