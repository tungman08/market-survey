export default {
  type: 'object',
  properties: {
    id: {
      description: 'รหัสการตอบ',
      type: 'string'
    },
    questionnaire: {
      description: 'รหัสแบบสอบถาม',
      type: 'string'
    },
    answers: {
      description: 'ข้อมูลคำตอบ',
      type: 'array',
      items: {
        type: 'object',
        properties: {
          id: {
            description: 'รหัสคำตอบ',
            type: 'string'
          },
          question: {
            description: 'รหัสคำถาม',
            type: 'string' 
          },
          value: {
            description: 'ข้อมูลคำตอบเป็นได้ทั้ง string, number, number[], number[][] หรือ datetime',
            type: 'string' 
          }
        }
      }
    }
  }
};
