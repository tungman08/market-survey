export default {
  get: {
    tags: [
      'users'
    ],
    summary: 'เรียกดูข้อมูลของผู้ใช้งานระบบด้วยรหัสผู้ใช้งาน',
    description: 'เรียกดูข้อมูลของผู้ใช้งานระบบด้วยรหัสผู้ใช้งาน',
    parameters: [
      {
        in: 'path',
        name: 'id',
        description: 'รหัสผู้ใช้งาน',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      200: {
        schema: {
          $ref: '#/definitions/User'
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      404: {
        schema: {
          $ref: '#/definitions/NotFound'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  },
  put: {
    tags: [
      'users'
    ],
    summary: 'แก้ไขข้อมูลผู้ใช้งานระบบ',
    description: 'แก้ไขข้อมูลผู้ใช้งานระบบ',
    parameters: [
      {
        in: 'path',
        name: 'id',
        description: 'รหัสผู้ใช้งาน',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'body',
        description: 'ข้อมูลที่ใช้สำหรับแก้ไขผู้ใช้งานระบบ',
        required: true,
        type: 'object',
        properties: {
          firstName: {
            description: 'ชื่อ',
            type: 'string',
            required: true
          },
          lastName: {
            description: 'นามสกุล',
            type: 'string',
            required: true
          },
          email: {
            description: 'อีเมล',
            type: 'string',
            required: true
          },
          active: {
            description: 'สถานะ',
            type: 'boolean',
            required: true
          },
          roles: {
            description: 'สิทธิ์การใช้ง่ร',
            type: 'array',
            items: {
              type: 'string',
              enum: [
                'ADMIN',
                'USER'
              ],
              default: 'USER'
            }
          }
        }
      }
    ],
    responses: {
      200: {
        schema: {
          $ref: '#/definitions/User'
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      404: {
        schema: {
          $ref: '#/definitions/NotFound'
        }
      },
      422: {
        schema: {
          $ref: '#/definitions/ValidationError'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  },
  delete: {
    tags: [
      'users'
    ],
    summary: 'ลบข้อมูลผู้ใช้งานระบบ',
    description: 'ลบข้อมูลผู้ใช้งานระบบ',
    parameters: [
      {
        in: 'path',
        name: 'id',
        description: 'รหัสผู้ใช้งาน',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      204: {
        description: 'No Content'
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  }
};
