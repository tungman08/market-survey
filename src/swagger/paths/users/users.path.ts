export default {
  get: {
    tags: [
      'users'
    ],
    summary: 'เรียกดูข้อมูลของผู้ใช้งานระบบทั้งหมด',
    description: 'เรียกดูข้อมูลของผู้ใช้งานระบบทั้งหมด',
    responses: {
      200: {
        schema: {
          type: 'array',
          items: {
            $ref: '#/definitions/User'
          }
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  },
  post: {
    tags: [
      'users'
    ],
    summary: 'สร้างผู้ใช้งานระบบ',
    description: 'สร้างผู้ใช้งานระบบ',
    parameters: [
      {
        in: 'body',
        name: 'body',
        description: 'ข้อมูลที่ใช้สำหรับสร้างผู้ใช้งานระบบ',
        required: true,
        type: 'object',
        properties: {
          username: {
            description: 'ชื่อผู้ใช้งานระบบ',
            type: 'string',
            required: true
          },
          password: {
            description: 'รหัสผ่าน',
            type: 'string',
            required: true
          },
          firstName: {
            description: 'ชื่อ',
            type: 'string',
            required: true
          },
          lastName: {
            description: 'นามสกุล',
            type: 'string',
            required: true
          },
          email: {
            description: 'อีเมล',
            type: 'string',
            required: true
          },
          roles: {
            description: 'สิทธิ์การใช้ง่ร',
            type: 'array',
            items: {
              type: 'string',
              enum: [
                'ADMIN',
                'USER'
              ],
              default: 'USER'
            }
          }
        }
      }
    ],
    responses: {
      200: {
        schema: {
          type: 'array',
          items: {
            $ref: '#/definitions/User'
          }
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      422: {
        schema: {
          $ref: '#/definitions/ValidationError'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  }
};
