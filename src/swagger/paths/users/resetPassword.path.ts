export default {
  post: {
    tags: [
      'users'
    ],
    summary: 'รีเซ็ตรหัสผ่านผู้ใช้งานระบบ',
    description: 'รีเซ็ตรหัสผ่านผู้ใช้งานระบบ',
    parameters: [
      {
        in: 'path',
        name: 'id',
        description: 'รหัสผู้ใช้งาน',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'body',
        description: 'ข้อมูลที่ใช้สำหรับแก้ไขผู้ใช้งานระบบ',
        required: true,
        type: 'object',
        properties: {
          password: {
            description: 'รหัสผ่านใหม่',
            type: 'string',
            required: true
          },
          secretAdmin: {
            description: 'ชื่อบัญชีของ secret admin',
            type: 'string',
            required: true
          },
          secretPassword: {
            description: 'รหัสผ่านของ secret admin',
            type: 'string',
            required: true
          }
        }
      }
    ],
    responses: {
      200: {
        schema: {
          $ref: '#/definitions/User'
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      404: {
        schema: {
          $ref: '#/definitions/NotFound'
        }
      },
      422: {
        schema: {
          $ref: '#/definitions/ValidationError'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  }
};
