export default {
  get: {
    tags: [
      'respondents'
    ],
    summary: 'เรียกดูข้อมูลคำตอบของแบบสอบถามของแต่ละผู้ตอบ',
    description: 'เรียกดูข้อมูลคำตอบของแบบสอบถามของแต่ละผู้ตอบ',
    parameters: [
      {
        in: 'path',
        name: 'questionnaireId',
        description: 'รหัสแบบสอบถาม',
        required: true,
        type: 'string'
      },
      {
        in: 'path',
        name: 'id',
        description: 'รหัสการตอบ',
        required: true,
        type: 'string'
      },
    ],
    responses: {
      200: {
        schema: {
          $ref: '#/definitions/Respondent'
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      404: {
        schema: {
          $ref: '#/definitions/NotFound'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  },
};
