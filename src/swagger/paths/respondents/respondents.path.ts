export default {
  get: {
    tags: [
      'respondents'
    ],
    summary: 'เรียกดูข้อมูลคำตอบทั้งหมดของแบบสอบถามที่ระบุ',
    description: 'เรียกดูข้อมูลคำตอบทั้งหมดของแบบสอบถามที่ระบุ',
    parameters: [
      {
        in: 'path',
        name: 'questionnaireId',
        description: 'รหัสแบบสอบถาม',
        required: true,
        type: 'string'
      },
    ],
    responses: {
      200: {
        schema: {
          type: 'array',
          items: {
            $ref: '#/definitions/Respondent'
          }
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      404: {
        schema: {
          $ref: '#/definitions/NotFound'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  },
};
