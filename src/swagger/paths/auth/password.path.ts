export default {
  post: {
    tags: [
      'auth'
    ],
    summary: 'เปลี่ยนรหัสผ่าน',
    description: 'เปลี่ยนรหัสผ่าน',
    parameters: [
      {
        in: 'body',
        name: 'body',
        description: 'ข้อมูลที่ใช้สำหรับเปลี่ยนรหัสผ่าน',
        required: true,
        type: 'object',
        properties: {
          oldPassword: {
            description: 'รหัสผ่านเดิม',
            type: 'string',
            required: true
          },
          password: {
            description: 'รหัสผ่าน',
            type: 'string',
            required: true
          }
        }
      }
    ],
    responses: {
      200: {
        schema: {
          $ref: '#/definitions/User'
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      422: {
        schema: {
          $ref: '#/definitions/ValidationError'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  }
};
