export default {
  post: {
    tags: [
      'auth'
    ],
    summary: 'รีเฟซโทเคน',
    description: 'รีเฟซโทเคน',
    parameters: [
      {
        in: 'body',
        name: 'body',
        description: 'ข้อมูลที่ใช้สำหรับขอโทเคนใหม่',
        required: true,
        type: 'object',
        properties: {
          accessToken: {
            description: 'โทเคน',
            type: 'string',
            required: true
          },
          refreshToken: {
            description: 'รีเฟซโทเคน',
            type: 'string',
            required: true
          }
        }
      }
    ],
    responses: {
      200: {
        schema: {
          $ref: '#/definitions/Token'
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      422: {
        schema: {
          $ref: '#/definitions/ValidationError'
        }
      }
    }
  }
};
