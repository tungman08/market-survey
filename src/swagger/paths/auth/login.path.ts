export default {
  post: {
    tags: [
      'auth'
    ],
    summary: 'เข้าสู่ระบบ',
    description: 'เข้าสู่ระบบ',
    parameters: [
      {
        in: 'body',
        name: 'body',
        description: 'ข้อมูลที่ใช้สำหรับเข้าสู่ระบบ',
        required: true,
        type: 'object',
        properties: {
          username: {
            description: 'ชื่อผู้ใช้งานระบบ',
            type: 'string',
            required: true
          },
          password: {
            description: 'รหัสผ่าน',
            type: 'string',
            required: true
          }
        }
      }
    ],
    responses: {
      200: {
        schema: {
          $ref: '#/definitions/Token'
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      422: {
        schema: {
          $ref: '#/definitions/ValidationError'
        }
      }
    }
  }
};
