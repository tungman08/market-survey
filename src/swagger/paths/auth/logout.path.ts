export default {
  post: {
    tags: [
      'auth'
    ],
    summary: 'ออกจากระบบ',
    description: 'ออกจากระบบ',
    responses: {
      204: {
        description: 'No Content'
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  }
};
