export default {
  get: {
    tags: [
      'auth'
    ],
    summary: 'แสดงข้อมูลผู้ใช้งาน',
    description: 'แสดงข้อมูลผู้ใช้งาน',
    responses: {
      200: {
        schema: {
          $ref: '#/definitions/User'
        }
      },
      422: {
        schema: {
          $ref: '#/definitions/ValidationError'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  },
  post: {
    tags: [
      'auth'
    ],
    summary: 'แก้ไขข้อมูลผู้ใช้งาน',
    description: 'แก้ไขข้อมูลผู้ใช้งาน',
    parameters: [
      {
        in: 'body',
        name: 'body',
        description: 'ข้อมูลที่ใช้สำหรับแก้ไขข้อมูลผู้ใช้งาน',
        required: true,
        type: 'object',
        properties: {
          firstName: {
            description: 'ชื่อ',
            type: 'string',
            required: true
          },
          lastName: {
            description: 'นามสกุล',
            type: 'string',
            required: true
          },
          email: {
            description: 'อีเมล',
            type: 'string',
            required: true
          },
        }
      }
    ],
    responses: {
      200: {
        schema: {
          $ref: '#/definitions/User'
        }
      },
      422: {
        schema: {
          $ref: '#/definitions/ValidationError'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  }
};