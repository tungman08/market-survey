export default {
  get: {
    tags: [
      'questionnaires'
    ],
    summary: 'เรียกดูข้อมูลแบบสอบถามด้วยรหัสแบบสอบถาม',
    description: 'เรียกดูข้อมูลแบบสอบถามด้วยรหัสแบบสอบถาม',
    parameters: [
      {
        in: 'path',
        name: 'id',
        description: 'รหัสแบบสอบถาม',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      200: {
        schema: {
          $ref: '#/definitions/Questionnaire'
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      404: {
        schema: {
          $ref: '#/definitions/NotFound'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  },
  put: {
    tags: [
      'questionnaires'
    ],
    summary: 'แก้ไขข้อมูลแบบสอบถาม',
    description: 'แก้ไขข้อมูลแบบสอบถาม',
    parameters: [
      {
        in: 'path',
        name: 'id',
        description: 'รหัสแบบสอบถาม',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'body',
        description: 'ข้อมูลที่ใช้สำหรับแก้ไขข้อมูลแบบสอบถาม',
        required: true,
        type: 'object',
        properties: {
          title: {
            description: 'ชื่อแบบสอบถาม',
            type: 'string',
            required: true
          },
          description: {
            description: 'บทนำ',
            type: 'string',
            required: true
          },
          releaseAt: {
            description: 'วันที่เผยแพร่',
            type: 'string',
            format: 'date',
            required: true
          },
          dueAt: {
            description: 'วันที่ปิดแบบสอบถาม',
            type: 'string',
            format: 'date',
            required: true
          }
        }
      }
    ],
    responses: {
      200: {
        schema: {
          $ref: '#/definitions/Questionnaire'
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      404: {
        schema: {
          $ref: '#/definitions/NotFound'
        }
      },
      422: {
        schema: {
          $ref: '#/definitions/ValidationError'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  },
  delete: {
    tags: [
      'questionnaires'
    ],
    summary: 'ลบข้อมูลแบบสอบถาม',
    description: 'ลบข้อมูลแบบสอบถาม',
    parameters: [
      {
        in: 'path',
        name: 'id',
        description: 'รหัสแบบสอบถาม',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      204: {
        description: 'No Content'
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  }
};
