export default {
  get: {
    tags: [
      'questionnaires'
    ],
    summary: 'เรียกดูข้อมูลสรุปข้อมูลคำตอบของแบบสอบถาม',
    description: 'เรียกดูข้อมูลสรุปข้อมูลคำตอบของแบบสอบถาม',
    parameters: [
      {
        in: 'path',
        name: 'id',
        description: 'รหัสแบบสอบถาม',
        required: true,
        type: 'string'
      },
    ],
    responses: {
      200: {
        schema: {
          $ref: '#/definitions/Result'
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      404: {
        schema: {
          $ref: '#/definitions/NotFound'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  },
};
