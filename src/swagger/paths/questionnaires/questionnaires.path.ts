export default {
  get: {
    tags: [
      'questionnaires'
    ],
    summary: 'เรียกดูข้อมูลแบบสอบถามทั้งหมด',
    description: 'เรียกดูข้อมูลแบบสอบถามทั้งหมด',
    responses: {
      200: {
        schema: {
          type: 'array',
          items: {
            $ref: '#/definitions/Questionnaire'
          }
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  },
  post: {
    tags: [
      'questionnaires'
    ],
    summary: 'สร้างแบบสอบถาม',
    description: 'สร้างแบบสอบถาม',
    parameters: [
      {
        in: 'body',
        name: 'body',
        description: 'ข้อมูลที่ใช้สำหรับสร้างแบบสอบถาม',
        required: true,
        type: 'object',
        properties: {
          title: {
            description: 'ชื่อแบบสอบถาม',
            type: 'string',
            required: true
          },
          description: {
            description: 'บทนำ',
            type: 'string',
            required: true
          },
          releaseAt: {
            description: 'วันที่เผยแพร่',
            type: 'string',
            format: 'date',
            required: true
          },
          dueAt: {
            description: 'วันที่ปิดแบบสอบถาม',
            type: 'string',
            format: 'date',
            required: true
          },

        }
      }
    ],
    responses: {
      200: {
        schema: {
          type: 'array',
          items: {
            $ref: '#/definitions/Questionnaire'
          }
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      422: {
        schema: {
          $ref: '#/definitions/ValidationError'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  }
};
