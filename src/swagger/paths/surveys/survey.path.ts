export default {
  get: {
    tags: [
      'surveys'
    ],
    summary: 'เรียกดูแบบฟอร์มแบบสอบถาม',
    description: 'เรียกดูแบบฟอร์มแบบสอบถาม',
    parameters: [
      {
        in: 'path',
        name: 'id',
        description: 'รหัสแบบสอบถาม',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      200: {
        schema: {
          $ref: '#/definitions/Survey'
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      404: {
        schema: {
          $ref: '#/definitions/NotFound'
        }
      }
    }
  },
  post: {
    tags: [
      'surveys'
    ],
    summary: 'ตอบแบบฟอร์มแบบสอบถาม',
    description: 'ตอบแบบฟอร์มแบบสอบถาม',
    parameters: [
      {
        in: 'path',
        name: 'id',
        description: 'รหัสแบบสอบถาม',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'body',
        description: 'ข้อมูลที่ใช้สำหรับตอบแบบสอบถาม ข้อมูลข้อคำตอบ (value) เป็นได้ทั้ง string, number[], number, number[][] หรือ datetime',
        required: true,
        type: 'array',
        items: {
          type: 'object',
          properties: {
            questionId: {
              description: 'รหัสคำถาม',
              type: 'string',
              required: true
            },
            questionType: {
              description: 'ประเภทข้อคำถาม',
              type: 'string',
              enum: [
                'TEXT',
                'OPTION',
                'RATING',
                'OPTIONGRID',
                'DATETIME'
              ],
              default: 'TEXT'
            },
            value: {
              description: 'ข้อมูลข้อคำตอบ เป็นได้ทั้ง string, number[], number, number[][] หรือ datetime',
              type: 'string',
            },
          }
        }
      }
    ],
    responses: {
      204: {
        description: 'No Content'
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      404: {
        schema: {
          $ref: '#/definitions/NotFound'
        }
      },
      422: {
        schema: {
          $ref: '#/definitions/ValidationError'
        }
      }
    },
  }
};