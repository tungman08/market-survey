export default {
  get: {
    tags: [
      'questions'
    ],
    summary: 'เรียกดูข้อคำถามด้วยรหัสข้อคำถาม',
    description: 'เรียกดูข้อคำถามด้วยรหัสข้อคำถาม',
    parameters: [
      {
        in: 'path',
        name: 'questionnaireId',
        description: 'รหัสแบบสอบถาม',
        required: true,
        type: 'string'
      },
      {
        in: 'path',
        name: 'id',
        description: 'รหัสข้อคำถาม',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      200: {
        schema: {
          $ref: '#/definitions/Question'
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      404: {
        schema: {
          $ref: '#/definitions/NotFound'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  },
  put: {
    tags: [
      'questions'
    ],
    summary: 'แก้ไขข้อคำถาม',
    description: 'แก้ไขข้อคำถาม',
    parameters: [
      {
        in: 'path',
        name: 'questionnaireId',
        description: 'รหัสแบบสอบถาม',
        required: true,
        type: 'string'
      },
      {
        in: 'path',
        name: 'id',
        description: 'รหัสแก้ไขข้อคำถาม',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'body',
        description: 'ข้อมูลที่ใช้สำหรับแก้ไขแก้ไขข้อคำถาม',
        required: true,
        type: 'object',
        properties: {
          required: {
            description: 'จำเป็นต้องกรอก',
            type: 'boolean',
            required: true
          },
          questionType: {
            description: 'ประเภทข้อคำถาม',
            type: 'string',
            items: {
              type: 'string',
              enum: [
                'TEXTBOX',
                'OPTION',
                'RATING',
                'OPTIONGRID',
                'DATETIME'
              ],
              default: 'TEXTBOX'
            }
          },
          questionBody: {
            description: 'ข้อมูลข้อคำถาม',
            type: 'object',
            properties: {
              text: {
                description: 'ข้อคำถาม',
                type: 'string'
              },
              isTextArea: {
                description: 'ใช้เป็น text area',
                type: 'boolean'
              }
            },
            required: true
          },
        }
      }
    ],
    responses: {
      200: {
        schema: {
          $ref: '#/definitions/Question'
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      404: {
        schema: {
          $ref: '#/definitions/NotFound'
        }
      },
      422: {
        schema: {
          $ref: '#/definitions/ValidationError'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  },
  delete: {
    tags: [
      'questions'
    ],
    summary: 'ลบข้อคำถาม',
    description: 'ลบข้อคำถาม',
    parameters: [
      {
        in: 'path',
        name: 'questionnaireId',
        description: 'รหัสแบบสอบถาม',
        required: true,
        type: 'string'
      },
      {
        in: 'path',
        name: 'id',
        description: 'รหัสข้อคำถาม',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      204: {
        description: 'No Content'
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  }
};
