export default {
  get: {
    tags: [
      'questions'
    ],
    summary: 'เรียกดูข้อคำถามทั้งหมดในแบบสอบถาม',
    description: 'เรียกดูข้อคำถามทั้งหมดในแบบสอบถาม',
    parameters: [
      {
        in: 'path',
        name: 'questionnaireId',
        description: 'รหัสแบบสอบถาม',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      200: {
        schema: {
          type: 'array',
          items: {
            $ref: '#/definitions/Question'
          }
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  },
  post: {
    tags: [
      'questions'
    ],
    summary: 'สร้างข้อคำถาม',
    description: 'สร้างข้อคำถาม',
    parameters: [
      {
        in: 'path',
        name: 'questionnaireId',
        description: 'รหัสแบบสอบถาม',
        required: true,
        type: 'string'
      },
      {
        in: 'body',
        name: 'body',
        description: 'ข้อมูลที่ใช้สำหรับสร้างข้อคำถาม (questionBody) เป็นได้ทั้ง QuestionTextBox, QuestionOption, QuestionRating, QuestionOptionGrid หรือ QuestionDateTime',
        required: true,
        type: 'object',
        properties: {
          required: {
            description: 'จำเป็นต้องกรอก',
            type: 'boolean',
            required: true
          },
          questionType: {
            description: 'ประเภทข้อคำถาม',
            type: 'string',
            items: {
              type: 'string',
              enum: [
                'TEXTBOX',
                'OPTION',
                'RATING',
                'OPTIONGRID',
                'DATETIME'
              ],
              default: 'TEXTBOX'
            }
          },
          questionBody: {
            description: 'ข้อมูลข้อคำถาม',
            type: 'object',
            properties: {
              text: {
                description: 'ข้อคำถาม',
                type: 'string'
              },
              isTextArea: {
                description: 'ใช้เป็น text area',
                type: 'boolean'
              }
            },
            required: true
          },
        }
      }
    ],
    responses: {
      200: {
        schema: {
          type: 'array',
          items: {
            $ref: '#/definitions/Question'
          }
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      422: {
        schema: {
          $ref: '#/definitions/ValidationError'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  }
};