export default {
  post: {
    tags: [
      'questions'
    ],
    summary: 'เลื่อนตำแหน่งข้อคำถามขึ้น',
    description: 'เลื่อนตำแหน่งข้อคำถามขึ้น',
    parameters: [
      {
        in: 'path',
        name: 'questionnaireId',
        description: 'รหัสแบบสอบถาม',
        required: true,
        type: 'string'
      },
      {
        in: 'path',
        name: 'id',
        description: 'รหัสข้อคำถาม',
        required: true,
        type: 'string'
      }
    ],
    responses: {
      204: {
        description: 'No Content'
      },
      400: {
        schema: {
          type: 'object',
          properties: {
            message: {
              type: 'string',
              example: 'Cannot Move.'
            },
          }
        }
      },
      401: {
        schema: {
          $ref: '#/definitions/Unauthorized'
        }
      },
      404: {
        schema: {
          $ref: '#/definitions/NotFound'
        }
      }
    },
    security: [
      {
        Bearer: []
      }
    ]
  }
};