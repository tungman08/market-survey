import { JsonObject } from 'swagger-ui-express';

/* paths */
import loginPath from './paths/auth/login.path';
import refreshPath from './paths/auth/refresh.path';
import logoutPath from './paths/auth/logout.path';
import profilePath from './paths/auth/profile.path';
import passwordPath from './paths/auth/password.path';
import usersPath from './paths/users/users.path';
import userPath from './paths/users/user.path';
import resetPasswordPath from './paths/users/resetPassword.path';
import questionnairesPath from './paths/questionnaires/questionnaires.path';
import questionnairePath from './paths/questionnaires/questionnaire.path';
import questionsPath from './paths/questions/questions.path';
import questionPath from './paths/questions/question.path';
import moveupPath from './paths/questions/moveup.path';
import movedownPath from './paths/questions/movedown.path';
import respondentsPath from './paths/respondents/respondents.path';
import respondentPath from './paths/respondents/respondent.path';
import resultPath from './paths/questionnaires/result.path';
import surveyPath from './paths/surveys/survey.path';

/* definitions */
import tokenDefinition from './definitions/token.definition';
import unauthorizedDefinition from './definitions/unauthorized.definition';
import notfoundDefinition from './definitions/notfound.definition';
import validationErrorDefinition from './definitions/validation-error.definition';
import internalErrorDefinition from './definitions/server-error.definition';
import userDefinition from './definitions/user.definition';
import questionnaireDefinition from './definitions/questionnaire.definition';
import questionDefinition from './definitions/question.definition';
import questionTextBoxDefinition from './definitions/question-textbox.definition';
import questionOptionDefinition from './definitions/question-option.definition';
import questionRatingDefinition from './definitions/question-rating.definition';
import questionLikertDefinition from './definitions/question-likert.definition';
import questionDateTimeDefinition from './definitions/question-datetime.definition';
import respondentDefinition from './definitions/respondent.definition';
import resultDefinition from './definitions/result.definition';
import surveyDefinition from './definitions/survey.definition';

const swaggerDoc: JsonObject = {
  swagger: '2.0',
  info: {
    title: 'NT Market Survey API',
    description: 'API สำหรับระบบแบบสอบถามอิเล็กทรอนิกส์',
    version: '1.0.0'
  },
  host: process.env.APP_URL,
  basePath: '/',
  produces: [
    'application/json'
  ],
  consumes: [
    'application/json'
  ],
  schemes: [
    'http',
    'https'
  ],
  securityDefinitions: {
    Bearer: {
      description: 'Example value: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjU5MmQwMGJhNTJjYjJjM..',
      type: 'apiKey',
      name: 'Authorization',
      in: 'header'
    }
  },
  tags: [
    { name: 'auth', description: 'สำหรับระบุตัวตนของผู้ใช้งานระบบ' },
    { name: 'users', description: 'สำหรับให้ผู้ดูแลระบบ (ADMIN) ใช้จัดการข้อมูลผู้ใช้งานระบบ' },
    { name: 'questionnaires', description: 'สำหรับให้ผู้ดูแลระบบ (ADMIN) ใช้จัดการข้อมูลแบบสอบถาม' },
    { name: 'questions', description: 'สำหรับให้ผู้ดูแลระบบ (ADMIN) ใช้จัดการข้อคำถามต่าง ๆ ในแบบสอบถาม' },
    { name: 'surveys', description: 'สำหรับให้บุคคลทั่วไปใช้ตอบแบบสอบถาม' },
    { name: 'respondents', description: 'สำหรับให้ผู้ดูแลระบบ (ADMIN) ใช้ดูข้อมูลผลการตอบแบบสอบถาม' }
  ],
  paths: {
    '/auth/login': loginPath,
    '/auth/refresh': refreshPath,
    '/auth/logout': logoutPath,
    '/auth/profile': profilePath,
    '/auth/password': passwordPath,
    '/users': usersPath,
    '/users/{id}': userPath,
    '/users/{id}/reset': resetPasswordPath,
    '/questionnaires': questionnairesPath,
    '/questionnaires/{id}': questionnairePath,
    '/questionnaires/{id}/result': resultPath,
    '/questionnaires/{questionnaireId}/questions': questionsPath,
    '/questionnaires/{questionnaireId}/questions/{id}': questionPath,
    '/questionnaires/{questionnaireId}/questions/{id}/moveup': moveupPath,
    '/questionnaires/{questionnaireId}/questions/{id}/movedown': movedownPath,
    '/surveys/{id}':surveyPath,
    '/questionnaires/{questionnaireId}/respondents': respondentsPath,
    '/questionnaires/{questionnaireId}/respondents/{id}': respondentPath
  },
  definitions: {
    Token: tokenDefinition,
    Unauthorized: unauthorizedDefinition,
    NotFound: notfoundDefinition,
    ValidationError: validationErrorDefinition,
    ServerError: internalErrorDefinition,
    User: userDefinition,
    Questionnaire: questionnaireDefinition,
    Question: questionDefinition,
    QuestionTextBox: questionTextBoxDefinition,
    QuestionOption: questionOptionDefinition,
    QuestionRating: questionRatingDefinition,
    QuestionLikert: questionLikertDefinition,
    QuestionDateTime: questionDateTimeDefinition,
    Respondent: respondentDefinition,
    Result: resultDefinition,
    Survey: surveyDefinition
  }
};

export default swaggerDoc;
