import passport from 'passport';
import passportLocal from 'passport-local';
import passportJWT from 'passport-jwt';
import { NativeError } from 'mongoose';
import bcrypt from 'bcrypt';
import userService from '../services/user.service';
import { User, UserDocument } from '../models/user.model';

const LocalStrategy = passportLocal.Strategy;
const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

passport.use(new LocalStrategy({
  usernameField: 'username',
  passwordField: 'password'
},
async (username, password, callback) => {
  try {
    const user = await userService.FindByUsername(username);
    if (!user || !user.active || !bcrypt.compareSync(password, user.password as string)) {
      return callback(false, false, { message: 'Incorrect username or password' });
    } else {
      return callback(false, user.toJSON());
    }
  } catch (error) {
    if (error instanceof Error) {
      return callback(error);
    }
  }
}));

passport.use(new JWTStrategy({
  jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
  secretOrKey: process.env.JWT_SECRET
},
async (jwtPayload, callback) => {
  try {
    const user = await userService.FindById(jwtPayload.id);

    if (user) {
      return callback(false, user.toJSON());
    } else {
      return callback(false, false);
    }      
  } catch (err) {
    return callback(err);
  }
}));

passport.serializeUser<any, any>((_, user, callback) => {
  callback(false, user);
});

passport.deserializeUser((id: string, callback) => {
  User.findById(id, (err: NativeError, user: UserDocument) => {
    callback(err, user.id);
  });
});

export default passport;
