import ioRedis, { Redis } from 'ioredis';

export const RdClient = (): Redis => {
  const client = new ioRedis({
    host: process.env.REDIS_HOST,
    port: Number(process.env.REDIS_PORT),
    password: process.env.REDIS_PASSWORD,
    db: 0
  });

  return client;
};
