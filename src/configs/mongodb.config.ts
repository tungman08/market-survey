import mongoose from 'mongoose';

const connectionString = () => {
  return `mongodb://${process.env.DB_USERNAME}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}?authSource=admin`;
};

export const DbConnect = async (): Promise<void> => {
  const uri = connectionString();
  const mongooseOpts = {
    useCreateIndex: true,
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
  };

  try {
    await mongoose.connect(uri, mongooseOpts);
    // console.log('\x1b[32m%s\x1b[0m', '[mongodb] successfully connected to the database.');
  } catch (err) {
    // console.log('\x1b[31m%s\x1b[0m', '[mongodb] error connecting to the database.');
    console.log(err);
    process.exit(1);
  }
};

export const DbDisconnect = async (): Promise<void> => {
  try {
    await mongoose.connection.close();
    // console.log('\x1b[32m%s\x1b[0m', '[mongodb] connection disconnected through app termination.');
  } catch (err) {
    console.log(err);
  }
};
