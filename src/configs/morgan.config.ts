import { Request, Response } from 'express';
import statusCode from 'http-status-codes';
import morgan from 'morgan';
import moment from 'moment-timezone';

morgan.token('date', () => moment().format('YYYY-MM-DD HH:mm'));
morgan.token('statusColor', (_: Request, res: Response) => {
  // get the status code if response written
  const headersSent = (typeof res.headersSent !== 'boolean') ? Boolean(res.header) : res.headersSent;
  const status = (headersSent) ? res.statusCode : '000';

  // get status color
  const color = status >= statusCode.INTERNAL_SERVER_ERROR ? 31 // 500 red
    : status >= statusCode.BAD_REQUEST ? 33 // 400 yellow
      : status >= statusCode.MULTIPLE_CHOICES ? 36 // 300 cyan
        : status >= statusCode.OK ? 32 // 200 green
          : 35; // 000 magenta

  return `\x1b[${color}m${status}\x1b[0m`;
});
morgan.format('custom', '[:date[Asia/Bangkok]] \x1b[36m:remote-addr\x1b[0m \x1b[33m:method\x1b[0m :url\x1b[0m :statusColor :response-time ms => :res[content-length]');

export default morgan('custom');
