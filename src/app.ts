import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import routes from './routes';
import logger from './configs/morgan.config';
import { Redis } from 'ioredis';
import errorMiddleware from './middlewares/error.middleware';

// custom express request interface
declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Express {
    interface Request {
      redis?: Redis
    }
  }
}

// helmet configulation
const cspDefaults = helmet.contentSecurityPolicy.getDefaultDirectives();
delete cspDefaults['upgrade-insecure-requests'];

const app = express();
app.enable('trust proxy');
app.use(cors({ origin: (_origin, callback) => callback(null, true), credentials: true }));
app.use(helmet({ contentSecurityPolicy: { directives: cspDefaults } }));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(logger);

// api routes
app.use(routes);

// global error handler
app.use(errorMiddleware);

export default app;
