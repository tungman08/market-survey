import mongoose, { Schema, Document } from 'mongoose';
import bcrypt from 'bcrypt';
import { Questionnaire, QuestionnaireDocument } from './questionnaire.model';

export enum Role {
  SecretAdmin = 'SECRET',
  Admin = 'ADMIN',
  User = 'USER'
}

export interface UserRequest {
  [key: string]: any;
}

export type UserDocument = Document & {
  username: string;
  password?: string;
  firstName: string;
  lastName: string;
  email: string;
  active: boolean;
  roles: Role[];
  passwordChanged: boolean;
  questionnaires: QuestionnaireDocument[],
};

const userSchema = new Schema<UserDocument>({
  username: { type: String, required: true, unique: true, lowercase: true },
  password: { type: String, required: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  email: { type: String, required: true },  
  active: { type: Boolean, required: true },
  roles: [
    { type: String, enum: [Role.SecretAdmin, Role.Admin, Role.User], default: Role.User }
  ],
  passwordChanged: { type: Boolean, required: true },
  questionnaires: [
    { type: Schema.Types.ObjectId, ref: 'Questionnaire' },
  ]
}, {
  timestamps: true,
  versionKey: false
});

userSchema.pre('save', function (next) {
  const user = this as UserDocument;
  const hash = bcrypt.hashSync(user.password, 10);
  user.password = hash;
  next();
});

userSchema.pre('deleteOne', async function (next) {
  const user = this as UserDocument;
  await Questionnaire.deleteMany({ user: user.id });
  next();
});

userSchema.set('toJSON', {
  virtuals: true,
  versionKey: false,
  transform: (_doc: Document, ret: UserDocument) => {
    // remove these props when object is serialized
    ret.id = ret._id;
    delete ret._id;
    delete ret.password;
    return ret;
  }
});

export const User = mongoose.model<UserDocument>('User', userSchema);