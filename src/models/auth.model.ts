export interface Token {
  accessToken: string,
  refreshToken: string,
  expiresIn: number,
  refreshExpiresIn: number
}

export interface AuthResult {
  statusCode: number,
  body: any
}
