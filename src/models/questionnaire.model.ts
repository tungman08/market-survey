import mongoose, { Schema, Document, Types } from 'mongoose';
import { Question, QuestionDocument } from './question.model';
import { Respondent, RespondentDocument } from './respondent.model';
import { User } from './user.model';

export interface QuestionnaireRequest {
  [key: string]: any;
}

export type QuestionnaireDocument = Document & {
  user: Types.ObjectId,
  title: string;
  description: string;
  releaseAt: Date;
  dueAt: Date;
  questions: QuestionDocument[],
  respondents: RespondentDocument[]
};

const questionnaireSchema = new Schema<QuestionnaireDocument>({
  user: { type: Schema.Types.ObjectId, ref: 'User' },
  title: { type: String, required: true },
  description: { type: String, required: true },
  releaseAt: { type: Date, required: true },
  dueAt: { type: Date, required: true },
  questions: [
    { type: Schema.Types.ObjectId, ref: 'Question' },
  ],
  respondents: [
    { type: Schema.Types.ObjectId, ref: 'Respondent' },
  ]
}, {
  timestamps: true,
  versionKey: false
});

questionnaireSchema.post('save', async function () {
  const questionnaire = this as QuestionnaireDocument;
  await User.updateOne({ _id: questionnaire.user }, { $push: { user: questionnaire.id } });
});

questionnaireSchema.pre('deleteOne', async function (next) {
  const questionnaire = this as QuestionnaireDocument;
  await Question.deleteMany({ questionnaire: questionnaire.id });
  await Respondent.deleteMany({ questionnaire: questionnaire.id });
  next();
});

questionnaireSchema.set('toJSON', {
  virtuals: true,
  versionKey: false,
  transform: (_doc: Document, ret: QuestionnaireDocument) => {
    // remove these props when object is serialized
    ret.id = ret._id;
    delete ret._id;
    return ret;
  }
});

export const Questionnaire = mongoose.model<QuestionnaireDocument>('Questionnaire', questionnaireSchema);
