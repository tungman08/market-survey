import mongoose, { Schema, Document, Types } from 'mongoose';
import { Questionnaire } from './questionnaire.model';
import { Answer, AnswerDocument } from './answer.model';

export interface RespondentRequest {
  [key: string]: any;
}

export type RespondentDocument = Document & {
  questionnaire: Types.ObjectId,
  answers: AnswerDocument[]
};

const respondentSchema = new Schema<RespondentDocument>({
  questionnaire: { type: Schema.Types.ObjectId, ref: 'Questionnaire' },
  answers: [
    { type: Schema.Types.ObjectId, ref: 'Answer' },
  ]
}, {
  timestamps: true,
  versionKey: false
});

respondentSchema.post('save', async function () {
  const respondent = this as RespondentDocument;
  await Questionnaire.updateOne({ _id: respondent.questionnaire }, { $push: { questions: respondent.id } });
});

respondentSchema.pre('deleteOne', async function (next) {
  const respondent = this as RespondentDocument;
  await Questionnaire.updateOne({ _id: respondent.questionnaire }, { $pull: { questions: respondent.id } });
  await Answer.deleteMany({ respondent: respondent.id });
  next();
});

respondentSchema.set('toJSON', {
  virtuals: true,
  versionKey: false,
  transform: (_doc: Document, ret: RespondentDocument) => {
    // remove these props when object is serialized
    ret.id = ret._id;
    delete ret._id;
    return ret;
  }
});

export const Respondent = mongoose.model<RespondentDocument>('Respondent', respondentSchema);
