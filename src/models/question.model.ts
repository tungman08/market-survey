import mongoose, { Schema, Document, Types } from 'mongoose';
import { Questionnaire } from './questionnaire.model';
import { Answer, AnswerDocument } from './answer.model';

export interface QuestionRequest {
  [key: string]: any;
}

export enum QuestionType {
  TextBox = 'TEXTBOX',
  Choice = 'CHOICE',
  Rating = 'RATING',
  Likert = 'LIKERT',
  DateTime = 'DATETIME',
}

export enum QuestionOptionType {
  DropDownList = 'DROPDOWNLIST',
  CheckBox = 'CHECKBOX',
  RadioBox = 'RADIOBOX'
}

export enum QuestionRatingSymbol {
  Star = 'STAR',
  Number = 'NUMBER',
}

export interface QuestionTextBox {
  name: QuestionType.TextBox,
  text: string,
  isTextArea: boolean
}

export interface QuestionChoice {
  name: QuestionType.Choice,
  text: string,
  optionType: QuestionOptionType,
  options: string[],
  hasOther: boolean
}

export interface QuestionRating {
  name: QuestionType.Rating,
  text: string,
  level: number,
  symbol: QuestionRatingSymbol,
  label: { show: boolean, start: string, end: string }
}

export interface QuestionLikert {
  name: QuestionType.Likert,
  text: string,
  isCheckBox: boolean,
  rows: string[],
  columns: string[]
}

export interface QuestionDateTime {
  name: QuestionType.DateTime,
  text: string,
  isTime: boolean
}

export type QuestionDocument = Document & {
  questionnaire: Types.ObjectId,
  required: boolean,
  score: number,
  questionType: QuestionType,
  questionBody: (QuestionTextBox | QuestionChoice | QuestionRating | QuestionLikert | QuestionDateTime),
  answers: AnswerDocument[]
};

const questionSchema = new Schema<QuestionDocument>({
  questionnaire: { type: Schema.Types.ObjectId, ref: 'Questionnaire' },
  required: { type: Boolean, required: true },
  questionType: { type: String, default: QuestionType.TextBox,
    enum: [
      QuestionType.TextBox,
      QuestionType.Choice,
      QuestionType.Rating,
      QuestionType.Likert,
      QuestionType.DateTime
    ]},
  score: { type: Number, required: true },
  questionBody: { type: Schema.Types.Mixed },
  answers: [
    { type: Schema.Types.ObjectId, ref: 'Answer' },
  ]
}, {
  timestamps: true,
  versionKey: false
});

questionSchema.post('save', async function () {
  const question = this as QuestionDocument;
  await Questionnaire.updateOne({ _id: question.questionnaire }, { $push: { questions: question.id } });
});

questionSchema.pre('deleteOne', async function (next) {
  const question = this as QuestionDocument;
  await Questionnaire.updateOne({ _id: question.questionnaire }, { $pull: { questions: question.id } });
  await Answer.deleteMany({ question: question.id });
  next();
});

questionSchema.set('toJSON', {
  virtuals: true,
  versionKey: false,
  transform: (_doc: Document, ret: QuestionDocument) => {
    // remove these props when object is serialized
    ret.id = ret._id;
    delete ret._id;
    return ret;
  }
});

export const Question = mongoose.model<QuestionDocument>('Question', questionSchema);
