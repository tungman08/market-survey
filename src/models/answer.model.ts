import mongoose, { Schema, Document, Types } from 'mongoose';
import { Question } from './question.model';
import { Respondent } from './respondent.model';

export interface AnswerRequest {
  [key: string]: any;
}

export type AnswerDocument = mongoose.Document & {
  respondent: Types.ObjectId,
  question: Types.ObjectId,
  value: (string | number | number[] | Array<number[]> | Date)
};

const answerSchema = new Schema<AnswerDocument>({
  respondent: { type: Schema.Types.ObjectId, ref: 'Respondent' },
  question: { type: Schema.Types.ObjectId, ref: 'Question' },
  value: { type: Schema.Types.Mixed }
}, {
  timestamps: true,
  versionKey: false
});

answerSchema.post('save', async function () {
  const answer = this as AnswerDocument;
  await Respondent.updateOne({ _id: answer.respondent }, { $push: { answers: answer.id } });
  await Question.updateOne({ _id: answer.question }, { $push: { answers: answer.id } });
});

answerSchema.pre('deleteOne', async function (next) {
  const answer = this as AnswerDocument;
  await Respondent.updateOne({ _id: answer.respondent }, { $pull: { answers: answer.id } });
  await Question.updateOne({ _id: answer.question }, { $pull: { answers: answer.id } });
  next();
});

answerSchema.set('toJSON', {
  virtuals: true,
  versionKey: false,
  transform: (_doc: Document, ret: AnswerDocument) => {
    // remove these props when object is serialized
    ret.id = ret._id;
    delete ret._id;
    return ret;
  }
});

export const Answer = mongoose.model<AnswerDocument>('Answer', answerSchema);
