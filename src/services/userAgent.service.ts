import { UAParser } from 'ua-parser-js';

export interface UserAgentService {
  Parser: (uaString: string) => UAParser.IResult
}

const Parser = (uaString: string) => {
  const parser = new UAParser();
  parser.setUA(uaString);

  return parser.getResult();
};

export default { Parser } as UserAgentService;
