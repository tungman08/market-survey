import { Types } from 'mongoose';
import { User, UserRequest, UserDocument, Role } from '../models/user.model';

export interface UserService {
  Create: (payload: UserRequest) => Promise<UserDocument>;
  Fetch: () => Promise<UserDocument[]>;
  Update: (id: string, payload: UserRequest) => Promise<UserDocument | null>;
  Delete: (id: string) => Promise<void>;
  FindById: (id: string) => Promise<UserDocument | null>;
  FindByUsername: (username: string) => Promise<UserDocument | null>;
  ChangePassword: (id: string, password: string, passwordChanged?: boolean) => Promise<UserDocument | null>;
}

// Create
const Create = async (payload: UserRequest): Promise<UserDocument> => {
  const user = new User({
    ...payload,
    active: true,
    passwordChanged: false
  });
  await user.save();
  return user;
};

// Fetch
const Fetch = async (): Promise<UserDocument[]> => {
  const users: Array<UserDocument> = await User.find({ roles: { $nin: [Role.SecretAdmin] } },
    null, { sort: { firstName: 1, lastName: 1 } });
  return users;
};

// Update
const Update = async (id: string, payload: UserRequest): Promise<UserDocument | null> => {
  const user = await User.findOneAndUpdate({ _id: Types.ObjectId(id) }, { $set: payload }, { new: true });
  return user;
};

// Delete
const Delete = async (id: string): Promise<void> => {
  await User.deleteOne({ _id: Types.ObjectId(id) });
};

// Find by id
const FindById = async (id: string): Promise<UserDocument | null> => {
  const user = await User.findOne({ _id: Types.ObjectId(id) });
  return user;
};

// Find by user name
const FindByUsername = async (username: string): Promise<UserDocument | null> => {
  const user = await User.findOne({ username: username });
  return user;
};

// Change password
const ChangePassword = async (id: string, password: string, passwordChanged: boolean = true): Promise<UserDocument | null> => {
  const user = await User.findOne({ _id: Types.ObjectId(id) });

  if (user) {
    user.password = password;
    user.passwordChanged = passwordChanged;
    await user.save();
  }
  
  return user;
};

export default { Create, Fetch, Update, Delete, FindById, FindByUsername, ChangePassword } as UserService;
