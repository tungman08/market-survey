import { Types } from 'mongoose';
import { Answer, AnswerRequest, AnswerDocument } from '../models/answer.model';

export interface AnswerService {
  Create: (respondentId: string, questionId: string, payload: AnswerRequest) => Promise<AnswerDocument>;
  Fetch: (respondentId: string, questionId: string) => Promise<AnswerDocument[]>;
  Update: (id: string, payload: AnswerRequest) => Promise<AnswerDocument | null>;
  Delete: (id: string) => Promise<void>;
  FindById: (id: string) => Promise<AnswerDocument | null>;
  FindByRespondentAndQuestion: (respondentId: string, questionId: string) => Promise<AnswerDocument | null>;
}

// Create
const Create = async (respondentId: string, questionId: string, payload: AnswerRequest): Promise<AnswerDocument> => {
  const answer = new Answer({
    respondent: respondentId,
    question: questionId,
    ...payload
  });
  await answer.save();
  return answer;
};

// Fetch
const Fetch = async (respondentId: string, questionId: string): Promise<AnswerDocument[]> => {
  const answers: Array<AnswerDocument> = await Answer.find({ 
    respondent: respondentId,
    question: questionId 
  }, null, { sort: { updatedAt: -1 } });
  return answers;
};

// Update
const Update = async (id: string, payload: AnswerRequest): Promise<AnswerDocument | null> => {
  const answer = await Answer.findOneAndUpdate({ _id: Types.ObjectId(id) }, { $set: payload }, { new: true });
  return answer;
};

// Delete
const Delete = async (id: string): Promise<void> => {
  await Answer.deleteOne({ _id: Types.ObjectId(id) });
};

// Find by id
const FindById = async (id: string): Promise<AnswerDocument | null> => {
  const answer = await Answer.findOne({ _id: Types.ObjectId(id) });
  return answer;
};

// Find by respondent and question
const FindByRespondentAndQuestion = async (respondentId: string, questionId: string): Promise<AnswerDocument | null> => {
  const answer = await Answer.findOne({ respondent: respondentId, question: questionId });
  return answer;
};

export default { Create, Fetch, Update, Delete, FindById, FindByRespondentAndQuestion } as AnswerService;
