import { Types } from 'mongoose';
import { Answer } from '../models/answer.model';
import { Question } from '../models/question.model';
import { Respondent } from '../models/respondent.model';
import { Questionnaire, QuestionnaireRequest, QuestionnaireDocument } from'../models/questionnaire.model';

export interface QuestionnaireService {
  Create: (userId: string, payload: QuestionnaireRequest) => Promise<QuestionnaireDocument>;
  Fetch: (userId: string) => Promise<QuestionnaireDocument[]>;
  Update: (id: string, payload: QuestionnaireRequest) => Promise<QuestionnaireDocument | null>;
  Delete: (id: string) => Promise<void>;
  FindById: (id: string) => Promise<QuestionnaireDocument | null>;
  GetResult: (id: string) => Promise<QuestionnaireDocument | null>;
  GetForm: (id: string) => Promise<QuestionnaireDocument | null>;
}

// Create
const Create = async (userId: string, payload: QuestionnaireRequest): Promise<QuestionnaireDocument> => {
  const questionnaire = new Questionnaire({
    user: userId,
    ...payload,
    questions: []
  });
  await questionnaire.save();
  return questionnaire;
};

// Fetch
const Fetch = async (userId: string): Promise<QuestionnaireDocument[]> => {
  const questionnaires = await Questionnaire.find({ user: userId }, null, { sort: { updatedAt: -1, title: 1 } });
  return questionnaires;
};

// Update
const Update = async (id: string, payload: QuestionnaireRequest): Promise<QuestionnaireDocument | null> => {
  const questionnaire = await Questionnaire.findOneAndUpdate({ _id: Types.ObjectId(id) }, { $set: payload }, { new: true });
  return questionnaire;
};

// Delete
const Delete = async (id: string): Promise<void> => {
  await Questionnaire.deleteOne({ _id: Types.ObjectId(id) });
};

// Find by id
const FindById = async (id: string): Promise<QuestionnaireDocument | null> => {
  const questionnaire = await Questionnaire.findOne({ _id: Types.ObjectId(id) });
  
  if (questionnaire) {
    // สแตมป์ updatedAt ไว้สำหรับ sort
    await Questionnaire.updateOne({ _id: Types.ObjectId(id) }, { $set: { title: questionnaire.title } });
  }

  return questionnaire;
};

// get questionnaire with questions and respondents
const GetResult = async (id: string): Promise<QuestionnaireDocument | null> => {
  const questionnaire = await Questionnaire.findOne({ _id: Types.ObjectId(id) })
    .populate({ path: 'questions', model: Question, options: { sort: { 'score': 1 } } })
    .populate({ path: 'respondents', modal: Respondent, options: { sort: { 'createdAt': -1 } },
      populate: { path: 'answers', model: Answer  }});

  return questionnaire;
};

// get questionnaire with questions
const GetForm = async (id: string): Promise<QuestionnaireDocument | null> => {
  const questionnaire = await Questionnaire.findOne({ _id: Types.ObjectId(id) })
    .populate({ path: 'questions', model: Question, options: { sort: { 'score': 1 }}});

  return questionnaire;
};

export default { Fetch, Create, Update, Delete, FindById, GetResult, GetForm } as QuestionnaireService;
