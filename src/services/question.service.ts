import { Types } from 'mongoose';
import { Question, QuestionRequest, QuestionDocument } from '../models/question.model';

export interface QuestionService {
  Create: (questionnaireId: string, payload: QuestionRequest) => Promise<QuestionDocument>;
  Fetch: (questionnaireId: string) => Promise<QuestionDocument[]>;
  Update: (id: string, payload: QuestionRequest) => Promise<QuestionDocument | null>;
  Delete: (id: string) => Promise<void>;
  Count: (questionnaireId: string) => Promise<number>;
  FindById: (id: string) => Promise<QuestionDocument | null>;
  FindByScore: (questionnaireId: string, score: number) => Promise<QuestionDocument | null>;
}

// Create
const Create = async (questionnaireId: string, payload: QuestionRequest): Promise<QuestionDocument> => {
  const count = await Count(questionnaireId);
  const question = new Question({
    questionnaire: questionnaireId,
    score: (count + 1) * 1024,
    ...payload
  });
  await question.save();
  return question;
};

// Fetch
const Fetch = async (questionnaireId: string): Promise<QuestionDocument[]> => {
  const questions = await Question.find({ questionnaire: questionnaireId }, null, { sort: { score: 1 } });
  return questions;
};

// Update
const Update = async (id: string, payload: QuestionRequest): Promise<QuestionDocument | null> => {
  const question = await Question.findOneAndUpdate({ _id: Types.ObjectId(id) }, { $set: payload }, { new: true });
  return question;
};

// Delete
const Delete = async (id: string): Promise<void> => {
  await Question.deleteOne({ _id: Types.ObjectId(id) });
};

// Count
const Count = async (questionnaireId: string): Promise<number> => {
  const count = await Question.countDocuments({ questionnaire: questionnaireId });
  return count;
};

// Find by id
const FindById = async (id: string): Promise<QuestionDocument | null> => {
  const question = await Question.findOne({ _id: Types.ObjectId(id) });
  return question;
};

// Find by score
const FindByScore = async (questionnaireId: string, score: number): Promise<QuestionDocument | null> => {
  const question = await Question.findOne({ questionnaire: questionnaireId, score: score });
  return question;
};

export default { Create, Fetch, Update, Delete, Count, FindById, FindByScore } as QuestionService;
