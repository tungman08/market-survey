import jwt from 'jsonwebtoken';
import randtoken from 'rand-token';
import { Redis } from 'ioredis';
import { Token } from '../models/auth.model';

export interface TokenService {
  Create: (redis: Redis, clientIp: string, browserName: string, userId: string) => Promise<Token>;
  Revoke: (redis: Redis, clientIp: string, browserName: string, userId: string) => Promise<void>;
  Find: (redis: Redis, clientIp: string, browserName: string, userId: string) => Promise<Token>;
}

const Create = async (redis: Redis, clientIp: string, browserName: string, userId: string): Promise<Token> => {
  // ลบ token
  await Revoke(redis, clientIp, browserName, userId);

  // สร้าง token และ refreshToken
  const token = createToken(userId);
  await redis.setex(`tokens:${clientIp}:${browserName.toLocaleLowerCase()}:${userId}`, token.expiresIn, token.accessToken);
  await redis.setex(`refreshs:${clientIp}:${browserName.toLocaleLowerCase()}:${userId}`, token.refreshExpiresIn, token.refreshToken);

  return token;
};

const Revoke = async (redis: Redis, clientIp: string, browserName: string, userId: string): Promise<void> => {
  // ลบ token
  await redis.del(`tokens:${clientIp}:${browserName.toLocaleLowerCase()}:${userId}`);
  await redis.del(`refreshs:${clientIp}:${browserName.toLocaleLowerCase()}:${userId}`);
};

const Find = async (redis: Redis, clientIp: string, browserName: string, userId: string): Promise<Token> => {
  // หา token ด้วย user id
  const accessToken = await redis.get(`tokens:${clientIp}:${browserName.toLocaleLowerCase()}:${userId}`);
  const refreshToken = await redis.get(`refreshs:${clientIp}:${browserName.toLocaleLowerCase()}:${userId}`);
  const expiresIn = await redis.ttl(`tokens:${clientIp}:${browserName.toLocaleLowerCase()}:${userId}`);
  const refreshExpiresIn = await redis.ttl(`refreshs:${clientIp}:${browserName.toLocaleLowerCase()}:${userId}`);

  return { 
    accessToken: accessToken,
    refreshToken: refreshToken,
    expiresIn: expiresIn,
    refreshExpiresIn: refreshExpiresIn
  } as Token;
};

const createToken = (userId: string): Token => {
  const accessToken = jwt.sign({ id: userId }, process.env.JWT_SECRET ?? '');
  const refreshToken = randtoken.uid(128);
  const expiresIn = Number(process.env.JWT_EXPIRATION);
  const refreshExpiresIn = Number(process.env.JWT_REFRESH_EXPIRATION);

  return {
    accessToken: accessToken,
    refreshToken: refreshToken,
    expiresIn: expiresIn,
    refreshExpiresIn: refreshExpiresIn
  } as Token;
};

export default { Create, Revoke, Find } as TokenService;
