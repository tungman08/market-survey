import { Types } from 'mongoose';
import { Respondent, RespondentRequest, RespondentDocument } from'../models/respondent.model';

export interface RespondentService {
  Create: (questionnaireId: string, payload: RespondentRequest) => Promise<RespondentDocument>;
  Fetch: (questionnaireId: string) => Promise<RespondentDocument[]>;
  Update: (id: string, payload: RespondentRequest) => Promise<RespondentDocument | null>;
  Delete: (id: string) => Promise<void>;
  FindById: (id: string) => Promise<RespondentDocument | null>;
}

// Create
const Create = async (questionnaireId: string, payload: RespondentRequest): Promise<RespondentDocument> => {
  const respondent = new Respondent({
    questionnaire: questionnaireId,
    ...payload
  });
  await respondent.save();
  return respondent;
};

// Fetch
const Fetch = async (questionnaireId: string): Promise<RespondentDocument[]> => {
  const respondents = await Respondent.find({ questionnaire: questionnaireId }, null, { sort: { updatedAt: -1 } });
  return respondents;
};

// Update
const Update = async (id: string, payload: RespondentRequest): Promise<RespondentDocument | null> => {
  const respondent = await Respondent.findOneAndUpdate({ _id: Types.ObjectId(id) }, { $set: payload }, { new: true });
  return await FindById(id);
};

// Delete
const Delete = async (id: string): Promise<void> => {
  await Respondent.deleteOne({ _id: Types.ObjectId(id) });
};

// Find by id
const FindById = async (id: string): Promise<RespondentDocument | null> => {
  const respondent = await Respondent.findOne({ _id: Types.ObjectId(id) });
  return respondent;
};

export default { Create, Fetch, Update, Delete, FindById } as RespondentService;
