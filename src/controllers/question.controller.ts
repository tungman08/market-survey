import { Request, Response } from 'express';
import statusCode from 'http-status-codes';
import { QuestionDocument } from '../models/question.model';
import { QuestionSchema } from '../validators/question.validator';
import questionService from '../services/question.service';

export interface QuestionController {
  Create: (req: Request, res: Response) => Promise<any>;
  Fetch: (req: Request, res: Response) => Promise<any>;
  Update: (req: Request, res: Response) => Promise<any>;
  Delete: (req: Request, res: Response) => Promise<any>;
  FindById: (req: Request, res: Response) => Promise<any>;
  MoveUp: (req: Request, res: Response) => Promise<any>;
  MoveDown: (req: Request, res: Response) => Promise<any>;
}

// Create
const Create = async (req: Request, res: Response): Promise<any> => {
  const { questionnaireId } = req.params;
  const questionRequest = req.body;
  const { error } = QuestionSchema.validate(questionRequest);

  if (error) {
    return res.status(statusCode.UNPROCESSABLE_ENTITY).json({ message: error.details[0].message });
  }

  try {
    const question = await questionService.Create(questionnaireId, questionRequest);
    return res.status(statusCode.CREATED).json(question.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Fetch
const Fetch = async (req: Request, res: Response): Promise<any> => {
  const { questionnaireId } = req.params;

  try {
    const questions = await questionService.Fetch(questionnaireId);
    return res.status(statusCode.OK).json(questions.map(question => question.toJSON()));
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Update
const Update = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.params;
  const questionRequest = req.body;
  const { error } = QuestionSchema.validate(questionRequest);

  if (error) {
    return res.status(statusCode.UNPROCESSABLE_ENTITY).json({ message: error.details[0].message });
  }

  try {
    const question = await questionService.Update(id, questionRequest);

    if (!question) {
      return res.status(statusCode.NOT_FOUND).json({ message: 'Not found' });
    } 

    return res.status(statusCode.OK).json(question.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Delete
const Delete = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.params;

  try {
    await questionService.Delete(id);
    return res.status(statusCode.NO_CONTENT).send();
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Find by id
const FindById = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.params;
  
  try {
    const question = await questionService.FindById(id);

    if (!question) {
      return res.status(statusCode.NOT_FOUND).json({ message: 'Not found' });
    }

    return res.status(statusCode.OK).json(question.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// move up
const MoveUp = async (req: Request, res: Response): Promise<any> => {
  const { questionnaireId, id } = req.params;

  try {
    const question = await questionService.FindById(id);

    if (!question) {
      return res.status(statusCode.NOT_FOUND).json({ message: 'Not found' });
    }

    const score = question.score;
    const upScore = score - 1024;
    const minScore = 1024;

    if (upScore < minScore) {
      return res.status(statusCode.BAD_REQUEST).json({ message: 'Cannot move' });
    }

    const upQuestion = await questionService.FindByScore(questionnaireId, upScore) as QuestionDocument;
    await questionService.Update(upQuestion.id, { score: score });
    await questionService.Update(question.id, { score: upScore });

    return res.status(statusCode.NO_CONTENT).send();
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Move down
const MoveDown = async (req: Request, res: Response): Promise<any> => {
  const { questionnaireId, id } = req.params;

  try {
    const question = await questionService.FindById(id);

    if (!question) {
      return res.status(statusCode.NOT_FOUND).json({ message: 'Not found' });
    }

    const score = question.score;
    const downScore = score + 1024;
    const count = await questionService.Count(questionnaireId);
    const maxScore = count * 1024;

    if (downScore > maxScore) {
      return res.status(statusCode.BAD_REQUEST).json({ message: 'Cannot move' });
    }

    const downQuestion = await questionService.FindByScore(questionnaireId, downScore) as QuestionDocument;
    await questionService.Update(downQuestion.id, { score: score });
    await questionService.Update(question.id, { score: downScore });

    return res.status(statusCode.NO_CONTENT).send();
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

export default { Create, Fetch, Update, Delete, FindById, MoveUp, MoveDown } as QuestionController;
