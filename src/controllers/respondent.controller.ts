import { Request, Response } from 'express';
import statusCode from 'http-status-codes';
import { RespondentSchema } from '../validators/respondent.validator';
import respondentService from '../services/respondent.service';
import { AnswerRequest } from '../models/answer.model';
import answerService from '../services/answer.service';

export interface RespondentController {
  Create: (req: Request, res: Response) => Promise<any>;
  Fetch: (req: Request, res: Response) => Promise<any>;
  Update: (req: Request, res: Response) => Promise<any>;
  Delete: (req: Request, res: Response) => Promise<any>;
  FindById: (req: Request, res: Response) => Promise<any>;
}

// Create
const Create = async (req: Request, res: Response): Promise<any> => {
  const { questionnaireId } = req.params;
  const respondentRequest = req.body;
  const { error } = RespondentSchema.validate(respondentRequest);
  
  if (error) {
    return res.status(statusCode.UNPROCESSABLE_ENTITY).json({ message: error.details[0].message });
  }

  try {
    const respondent = await respondentService.Create(questionnaireId, {});
    const answers = respondentRequest.answers as AnswerRequest[];
    await Promise.all(answers.map(async (answer) => {
      await answerService.Create(respondent.id, answer.question, { value: answer.value });
    }));

    return res.status(statusCode.CREATED).json(respondent.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Fetch
const Fetch = async (req: Request, res: Response): Promise<any> => {
  const { questionnaireId } = req.params;

  try {
    const respondents = await respondentService.Fetch(questionnaireId);
    return res.status(statusCode.OK).json(respondents.map(respondent => respondent.toJSON()));  
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Update
const Update = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.params;
  const respondentRequest = req.body;
  const { error } = RespondentSchema.validate(respondentRequest);

  if (error) {
    return res.status(statusCode.UNPROCESSABLE_ENTITY).json({ message: error.details[0].message });
  }

  try {
    const respondent = await respondentService.FindById(id);
    const answers = respondentRequest.answers as AnswerRequest[];
    await Promise.all(answers.map(async (answer) => {
      const response = await answerService.FindByRespondentAndQuestion(respondent?.id, answer.question);  
      await answerService.Update(response?.id, { value: answer.value });
    }));

    if (!respondent) {
      return res.status(statusCode.NOT_FOUND).json({ message: 'Not found' });
    } 
    
    return res.status(statusCode.OK).json(respondent.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Delete
const Delete = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.params;

  try {
    await respondentService.Delete(id);
    return res.status(statusCode.NO_CONTENT).send();
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  } 
};

// Find by id
const FindById = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.params;
  
  try {
    const respondent = await respondentService.FindById(id);

    if (!respondent) {
      return res.status(statusCode.NOT_FOUND).json({ message: 'Not found' });
    }

    return res.status(statusCode.OK).json(respondent.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

export default { Create, Fetch, Update, Delete, FindById } as RespondentController;
