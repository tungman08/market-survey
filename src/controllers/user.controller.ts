import { Request, Response } from 'express';
import statusCode from 'http-status-codes';
import bcrypt from 'bcrypt';
import { UserDocument } from '../models/user.model';
import userService from '../services/user.service';
import { CreateUserSchema, UpdateUserSchema, ResetPasswordSchema } from '../validators/user.validator';

export interface UserController {
  Create: (req: Request, res: Response) => Promise<any>;
  Fetch: (req: Request, res: Response) => Promise<any>;
  Update: (req: Request, res: Response) => Promise<any>;
  Delete: (req: Request, res: Response) => Promise<any>;
  FindById: (req: Request, res: Response) => Promise<any>;
  ResetPassword: (req: Request, res: Response) => Promise<any>;
}

// Create
const Create = async (req: Request, res: Response): Promise<any> => {
  const userRequest = req.body;
  const { error } = CreateUserSchema.validate(userRequest);

  if (error) {
    return res.status(statusCode.UNPROCESSABLE_ENTITY).json({ message: error.details[0].message });
  }

  try {
    const user = await userService.Create(userRequest);
    return res.status(statusCode.CREATED).json(user.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }    
};

// Fetch
const Fetch = async (_: Request, res: Response): Promise<any> => {
  try {
    const users = await userService.Fetch();
    return res.status(statusCode.OK).json(users.map(user => user.toJSON()));
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }  
};


// Update
const Update = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.params;
  const userRequest = req.body;
  const { error } = UpdateUserSchema.validate(userRequest);
  
  if (error) {
    return res.status(statusCode.UNPROCESSABLE_ENTITY).json({ message: error.details[0].message });
  }

  try {
    const user = await userService.Update(id, userRequest);

    if (!user) {
      return res.status(statusCode.NOT_FOUND).json({ message: 'Not found' });
    } 

    return res.status(statusCode.OK).json(user.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Delelte
const Delete = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.params;

  try {
    await userService.Delete(id);
    return res.status(statusCode.NO_CONTENT).send();
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  } 
};

// Find by Id
const FindById = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.params;

  try {
    const user = await userService.FindById(id);

    if (!user) {
      return res.status(statusCode.NOT_FOUND).json({ message: 'Not found' });
    } 

    return res.status(statusCode.OK).json(user.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Reset password
const ResetPassword = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.params;
  const userRequest = req.body;
  const { error } = ResetPasswordSchema.validate(userRequest);

  if (error) {
    return res.status(statusCode.UNPROCESSABLE_ENTITY).json({ message: error.details[0].message });
  }
  
  const secretAdmin = await userService.FindByUsername(userRequest.secretAdmin);

  if (!secretAdmin) {
    return res.status(statusCode.UNPROCESSABLE_ENTITY).json({ message: 'Secret admin not found' });
  }

  const isCorrect = bcrypt.compareSync(userRequest.secretPassword, secretAdmin.password as string);
  
  if (!isCorrect) {
    return res.status(statusCode.UNPROCESSABLE_ENTITY).json({ message: 'Secret admin password incorrect' });
  }

  try {
    const user = await userService.ChangePassword(id, userRequest.password, false);

    if (!user) {
      return res.status(statusCode.NOT_FOUND).json({ message: 'Not found' });
    } 

    return res.status(statusCode.OK).json(user.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

export default { Create, Fetch, Update, Delete, FindById, ResetPassword } as UserController;
