import { Request, Response } from 'express';
import questionnaireController from '../controllers/questionnaire.controller';
import respondentController from '../controllers/respondent.controller';

export interface SurveyController {
  GetForm: (req: Request, res: Response) => Promise<any>;
  Create: (req: Request, res: Response) => Promise<any>;
}

// GetForm
const GetForm = async (req: Request, res: Response): Promise<any> => {
  return await questionnaireController.GetForm(req, res);
};

// Create
const Create = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.params;
  req.params.questionnaireId = id;

  return await respondentController.Create(req, res);
};

export default { GetForm, Create } as SurveyController;
