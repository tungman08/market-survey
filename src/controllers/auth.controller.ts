import { Request, Response } from 'express';
import statusCode from 'http-status-codes';
import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import requestIp from 'request-ip';
import { RdClient } from '../configs/redis.config';
import authen from '../configs/passport.config';
import { CredentialsSchema, TokenSchema, PasswordSchema, ProfileSchema } from '../validators/auth.validator';
import { AuthResult, Token } from '../models/auth.model';
import { UserDocument } from '../models/user.model';
import tokenService from '../services/token.service';
import userAgentService from '../services/userAgent.service';
import userService from '../services/user.service';

export interface AuthController {
  Login: (req: Request, res: Response) => Promise<any>;
  Logout: (req: Request, res: Response) => Promise<any>;
  Refresh: (req: Request, res: Response) => Promise<any>;
  Profile: (req: Request, res: Response) => any;
  ChangePassword: (req: Request, res: Response) => Promise<any>;
  UpdateProfile: (req: Request, res: Response) => Promise<any>;
}

// Login
const Login = async (req: Request, res: Response): Promise<any> => {
  const credentialsRequest = req.body;
  const { error } = CredentialsSchema.validate(credentialsRequest);

  if (error) {
    return res.status(statusCode.UNPROCESSABLE_ENTITY).json({ message: error.details[0].message });
  }

  try {
    const result = await authenticate(req, res);
    return res.status(result.statusCode).json(result.body);
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Logout
const Logout = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.user as UserDocument;
  const clientIp = requestIp.getClientIp(req) as string;
  const browserName = getBrowserName(req.headers['user-agent']);
  const redis =  req.redis ?? RdClient();

  try {
    await tokenService.Revoke(redis, clientIp, browserName, id);
    return res.status(statusCode.NO_CONTENT).send();
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Refresh
const Refresh = async (req: Request, res: Response): Promise<any> => {
  const tokenRequest = req.body;
  const { error } = TokenSchema.validate(tokenRequest);

  if (error) {
    return res.status(statusCode.UNPROCESSABLE_ENTITY).json({ message: error.details[0].message });
  }

  try {
    const result = await authenticateRefreshToken(req, res);
    return res.status(result.statusCode).json(result.body);
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Profile
const Profile = (req: Request, res: Response): any => {
  try {
    return res.status(statusCode.OK).json(req.user);
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Update Profile
const UpdateProfile = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.user as UserDocument;
  const profileRequest = req.body;

  const user = await userService.FindById(id) as UserDocument;
  const { error } = ProfileSchema.validate(profileRequest);

  if (error) {
    return res.status(statusCode.UNPROCESSABLE_ENTITY).json({ message: error.details[0].message });
  } 

  try {
    const userDocument = await userService.Update(user.id, profileRequest);
    return res.status(statusCode.OK).json(userDocument?.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Change Password
const ChangePassword = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.user as UserDocument;
  const passwordRequest = req.body;
  const { error } = PasswordSchema.validate(passwordRequest);

  if (error) {
    return res.status(statusCode.UNPROCESSABLE_ENTITY).json({ message: error.details[0].message });
  }

  const user = await userService.FindById(id) as UserDocument;
  const isCorrect = bcrypt.compareSync(passwordRequest.oldPassword, user.password as string);

  if (!isCorrect) {
    return res.status(statusCode.UNPROCESSABLE_ENTITY).json({ message: 'Password incorrect' });
  }

  try {
    const userDocument = await userService.ChangePassword(user.id, passwordRequest.password);

    if (!userDocument) {
      return res.status(statusCode.NOT_FOUND).json({ message: 'Not found' });
    } 

    return res.status(statusCode.OK).json(userDocument.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

const authenticate = (req: Request, res: Response) => {
  return new Promise<AuthResult>((resolve, reject) => {
    authen.authenticate('local', { session: false }, async (error: Error, user: UserDocument, info: { message: string }) => {
      if (error) {
        reject({
          statusCode: statusCode.INTERNAL_SERVER_ERROR,
          body: { message: error.message }
        });
      } else if (!user) {
        resolve({
          statusCode: statusCode.UNAUTHORIZED,
          body: { message: info.message }
        });
      } else {
        const clientIp = requestIp.getClientIp(req) as string;
        const browserName = getBrowserName(req.headers['user-agent']);
        const redis =  req.redis ?? RdClient();
        const accessToken = await tokenService.Create(redis, clientIp, browserName, user.id);
        const token: Token = accessToken;

        resolve({ 
          statusCode: statusCode.OK,
          body: token
        });
      }
    })(req, res);
  });
};

const authenticateRefreshToken = (req: Request, _: Response) => {
  const { accessToken, refreshToken } = req.body;
  const decoded = jwt.decode(accessToken, {complete: true});
  const userId = decoded?.payload.id;
  const clientIp = requestIp.getClientIp(req) as string;
  const browserName = getBrowserName(req.headers['user-agent']);
  const redis =  req.redis ?? RdClient();

  return new Promise<AuthResult>((resolve, reject) => {
    tokenService.Find(redis, clientIp, browserName, userId)
      .then(async (token) => {
        if (token.refreshToken === refreshToken) {
          const accessToken = await tokenService.Create(redis, clientIp, browserName, userId);
          const token: Token = accessToken;
  
          resolve({ 
            statusCode: statusCode.OK,
            body: token 
          });
        } else {
          resolve({
            statusCode: statusCode.UNAUTHORIZED,
            body: { message: 'Unauthorized' }
          });
        }
      })
      .catch((error) => {
        reject({
          statusCode: statusCode.INTERNAL_SERVER_ERROR,
          body: { message: error.message }
        });
      });
  });
};

const getBrowserName = (uaString: string | undefined) => {
  const userAgent = userAgentService.Parser(uaString as string);
  return userAgent.browser.name as string;
};

export default { Login, Refresh, Logout, ChangePassword, Profile, UpdateProfile } as AuthController;
