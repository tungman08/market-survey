import { Request, Response } from 'express';
import statusCode from 'http-status-codes';
import { QuestionnaireSchema } from '../validators/questionnaire.validator';
import { UserDocument } from '../models/user.model';
import questionnaireService from '../services/questionnaire.service';

export interface QuestionnaireController {
  Create: (req: Request, res: Response) => Promise<any>;
  Fetch: (req: Request, res: Response) => Promise<any>;
  Update: (req: Request, res: Response) => Promise<any>;
  Delete: (req: Request, res: Response) => Promise<any>;
  FindById: (req: Request, res: Response) => Promise<any>;
  GetResult: (req: Request, res: Response) => Promise<any>;
  GetForm: (req: Request, res: Response) => Promise<any>;
}

// Create
const Create = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.user as UserDocument;
  const questionnaireRequest = req.body;
  const { error } = QuestionnaireSchema.validate(questionnaireRequest);

  if (error) {
    return res.status(statusCode.UNPROCESSABLE_ENTITY).json({ message: error.details[0].message });
  }
    
  try {
    const questionnaire = await questionnaireService.Create(id, questionnaireRequest);
    return res.status(statusCode.CREATED).json(questionnaire.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Fetch
const Fetch = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.user as UserDocument;

  try {
    const questionnaires = await questionnaireService.Fetch(id);
    return res.status(statusCode.OK).json(questionnaires.map(questionnaire => questionnaire.toJSON()));
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Update
const Update = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.params;
  const questionnaireRequest = req.body;
  const { error } = QuestionnaireSchema.validate(questionnaireRequest);

  if (error) {
    return res.status(statusCode.UNPROCESSABLE_ENTITY).json({ message: error.details[0].message });
  }

  try {
    const questionnaire = await questionnaireService.Update(id, questionnaireRequest);

    if (!questionnaire) {
      return res.status(statusCode.NOT_FOUND).json({ message: 'Not found' });
    } 

    return res.status(statusCode.OK).json(questionnaire.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Delete
const Delete = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.params;

  try {
    await questionnaireService.Delete(id);
    return res.status(statusCode.NO_CONTENT).send();
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  } 
};

// Find by id
const FindById = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.params;

  try {
    const questionnaire = await questionnaireService.FindById(id);

    if (!questionnaire) {
      return res.status(statusCode.NOT_FOUND).json({ message: 'Not found' });
    }

    return res.status(statusCode.OK).json(questionnaire.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Get questionnaire result with questions and respondents
const GetResult = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.params;

  try {
    const questionnaire = await questionnaireService.GetResult(id);

    if (!questionnaire) {
      return res.status(statusCode.NOT_FOUND).json({ message: 'Not found' });
    }

    return res.status(statusCode.OK).json(questionnaire.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

// Get questionnaire form with questions
const GetForm = async (req: Request, res: Response): Promise<any> => {
  const { id } = req.params;

  try {
    const questionnaire = await questionnaireService.GetForm(id);

    if (!questionnaire) {
      return res.status(statusCode.NOT_FOUND).json({ message: 'Not found' });
    }

    return res.status(statusCode.OK).json(questionnaire.toJSON());
  } catch (error: unknown) {
    if (error instanceof Error) {
      return res.status(statusCode.INTERNAL_SERVER_ERROR).json({ message: error.message });
    }
  }
};

export default { Create, Fetch, Update, Delete, FindById, GetResult, GetForm } as QuestionnaireController;
