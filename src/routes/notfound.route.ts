import { Router, Request, Response } from 'express';
import statusCode from 'http-status-codes';

const router = Router();
router.get('/', (_: Request, res: Response) => res.status(statusCode.NOT_FOUND).json({ message: 'not found' }));

export default router;