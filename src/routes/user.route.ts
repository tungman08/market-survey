import { Router } from 'express';
import userController from '../controllers/user.controller';
import secretMiddleware from '../middlewares/secret.middleware';

const router = Router();
router.get('/', userController.Fetch);
router.post('/', userController.Create);
router.get('/:id', userController.FindById);
router.put('/:id', userController.Update);
router.delete('/:id', userController.Delete);
router.post('/:id/reset', secretMiddleware, userController.ResetPassword);

export default router;
