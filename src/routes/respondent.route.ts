import { Router } from 'express';
import respondentController from '../controllers/respondent.controller';

const router = Router({ mergeParams: true });
router.get('/', respondentController.Fetch);
router.post('/', respondentController.Create);
router.get('/:id', respondentController.FindById);
router.put('/:id', respondentController.Update);
router.delete('/:id', respondentController.Delete);

export default router;