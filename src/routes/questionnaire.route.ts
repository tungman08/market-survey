import { Router } from 'express';
import questionnaireController from '../controllers/questionnaire.controller';

const router = Router();
router.get('/', questionnaireController.Fetch);
router.post('/', questionnaireController.Create);
router.get('/:id', questionnaireController.FindById);
router.put('/:id', questionnaireController.Update);
router.delete('/:id', questionnaireController.Delete);
router.get('/:id/result', questionnaireController.GetResult);

export default router;
