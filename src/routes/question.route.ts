import { Router } from 'express';
import questionController from '../controllers/question.controller';

const router = Router({ mergeParams: true });
router.get('/', questionController.Fetch);
router.post('/', questionController.Create);
router.get('/:id', questionController.FindById);
router.put('/:id', questionController.Update);
router.delete('/:id', questionController.Delete);
router.post('/:id/moveup', questionController.MoveUp);
router.post('/:id/movedown', questionController.MoveDown);

export default router;