import { Router } from 'express';
import swaggerRoute from '../routes/swagger.route';
import authRoute from '../routes/auth.route';
import userRoute from '../routes/user.route';
import questionnaireRoute from '../routes/questionnaire.route';
import questionRoute from '../routes/question.route';
import respondentRoute from './respondent.route';
import surveyRoute from './survey.route';
import notfoundRoute from '../routes/notfound.route';

// import middlewares
import authMiddleware from '../middlewares/auth.middleware';
import adminMiddleware from '../middlewares/admin.middleware';
import userMiddleware from '../middlewares/user.middleware';
import redisMiddleware from '../middlewares/redis.middleware';

const router = Router();
router.use('/', swaggerRoute);
router.use('/auth', redisMiddleware, authRoute);
router.use('/users', [authMiddleware, adminMiddleware], userRoute);
router.use('/questionnaires', [authMiddleware, userMiddleware], questionnaireRoute);
router.use('/questionnaires/:questionnaireId/questions', [authMiddleware, userMiddleware], questionRoute);
router.use('/questionnaires/:questionnaireId/respondents', [authMiddleware, userMiddleware], respondentRoute);
router.use('/surveys', surveyRoute);
router.get('*', notfoundRoute);

export default router;
