import { Router } from 'express';
import authController from '../controllers/auth.controller';
import authMiddleware from '../middlewares/auth.middleware';

const router = Router();
router.post('/login', authController.Login);
router.post('/refresh', authController.Refresh);
router.post('/logout', authMiddleware, authController.Logout);
router.get('/profile', authMiddleware, authController.Profile);
router.post('/profile', authMiddleware, authController.UpdateProfile);
router.post('/password', authMiddleware, authController.ChangePassword);

export default router;