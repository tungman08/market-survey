import { Router } from 'express';
import surveyController from '../controllers/survey.controller';

const router = Router({ mergeParams: true });
router.get('/:id', surveyController.GetForm);
router.post('/:id', surveyController.Create);

export default router;