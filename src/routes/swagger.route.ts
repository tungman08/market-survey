import { Router, Request, Response } from 'express';
import swaggerUi from 'swagger-ui-express';
import swaggerDoc from '../swagger/api-docs';

const router = Router();
router.get('/', (_: Request, res: Response) => res.redirect('/api-docs'));
router.use('/api-docs', swaggerUi.serve);
router.get('/api-docs', swaggerUi.setup(swaggerDoc));

export default router;
