import Joi from 'joi';

export const QuestionnaireSchema = Joi.object({
  title: Joi.string().required(),
  description: Joi.string().required(),
  releaseAt: Joi.date().required(),
  dueAt: Joi.date().min(Joi.ref('releaseAt')).required()
});
