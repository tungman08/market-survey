import Joi from 'joi';

export const RespondentSchema = Joi.object({
  answers: Joi.array().items(
    Joi.object({
      question: Joi.string().required(),
      value: Joi.alternatives().try(
        Joi.string(),
        Joi.number(),
        Joi.array().items(Joi.number()),
        Joi.array().items(Joi.array().items(Joi.number())),
        Joi.date()
      ).required()
    }).min(1).required()
  )
});
