import Joi from 'joi';

export const CredentialsSchema = Joi.object({
  username: Joi.string().required(),
  password: Joi.string().required()
});

export const TokenSchema = Joi.object({
  accessToken: Joi.string().required(),
  refreshToken: Joi.string().required()
});

export const PasswordSchema = Joi.object({
  oldPassword: Joi.string().required(),
  password: Joi.string().required()
});

export const ProfileSchema = Joi.object({
  firstName: Joi.string().required(),
  lastName: Joi.string().required(),
  email: Joi.string()
    .pattern(new RegExp(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/))
    .required(),
});
